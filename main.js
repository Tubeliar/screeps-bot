const managers = require("manager._all");
const planner = require("planner");
const taskTypes = require("task._all");
const specialisms = require("specialist._all");
const util = require("util");
require("prototype.creep")();
require("prototype.structure")();

module.exports.loop = function()
{
	if (!Memory)
	{
		if (Game.time % 10 == 0) console.log("No memory object!");
		return;
	}
	let t0 = Game.cpu.getUsed();
	
	if (!Memory.config)
	{
		Memory.config =
		{
			enableExpansion: true,
			enableScout: true,
			scoutRadius: 3,
			trackEmployment: false,
			monitor: null,
			showTiming: false
		};
	}
	for (const type in managers)
	{
		managers[type].update();
	}
	
	for (const roomName in Game.rooms)
	{
		for (const structure of Game.rooms[roomName].find(FIND_STRUCTURES))
		{
			structure.resetFuture();
		}
	}
	
	if (Memory.clearFlags)
	{
		util.clearFlags();
		delete Memory.clearFlags;
	}
	let t1 = Game.cpu.getUsed();
	
	// Drive specialists
	for (const specialismName in specialisms)
	{
		specialisms[specialismName].spawn();
	}
	for (const creepName in Game.creeps)
	{
		const creep = Game.creeps[creepName];
		if (creep.memory.specialism) specialisms[creep.memory.specialism].run(creep);
	}
	
	// Drive workers
	let t2 = Game.cpu.getUsed();
	for (const creepName in Game.creeps)
	{
		const creep = Game.creeps[creepName];
		const rememberedTask = creep.memory.currentTask;
		if (rememberedTask)
		{
			if (rememberedTask && rememberedTask.class)
			{
				let task = taskTypes[rememberedTask.class].revive(rememberedTask);
				task.perform(creep);
			}
			else console.log("No class? " + JSON.stringify(rememberedTask));
		}
	}
	let t2a = Game.cpu.getUsed();
	const plans = planner.getPlans();
	let t3 = Game.cpu.getUsed();
	for (const name in plans)
	{
		const creep = Game.creeps[name];
		const task = plans[name];
		const cTask = creep.memory.currentTask;
		if (!cTask || cTask.class != task.class || cTask.targetId != task.targetId)
		{
			task.say(creep);
			creep.memory.currentTask = task;
		}
		task.prepare(creep);
	}
	
	let t4 = Game.cpu.getUsed();
	// Drive towers
	for (const roomName in Memory.rooms.owned)
	{
		Game.rooms[roomName].find(FIND_MY_STRUCTURES, {filter: {structureType: STRUCTURE_TOWER}}).forEach(function(tower)
		{
			const closestHostile = tower.pos.findClosestByRange(FIND_HOSTILE_CREEPS);
			if (closestHostile)
			{
				tower.attack(closestHostile);
			}
		});
	}
	
	let t5 = Game.cpu.getUsed();
	// Finish up
	for (const type in managers)
	{
		if (managers[type].lateUpdate) managers[type].lateUpdate();
	}
	
	let t6 = Game.cpu.getUsed();
	if (Memory.config.showTiming || (t6 - t0 > 150))
	{
		console.log(
			"Total: " + util.width(t6 - t0, 6) +
			" managers: " + util.width(t1 - t0, 6) +
			" special: " + util.width(t2 - t1, 6) +
			" finish: " + util.width(t2a - t2, 6) +
			" planning: " + util.width(t3 - t2a, 6) +
			" workers: " + util.width(t4 - t3, 6) +
			" towers: " + util.width(t5 - t4, 6) +
			" lateUpdate: " + util.width(t6 - t5, 6)
		);
	}
};