
module.exports =
{
	rooms: require("manager.rooms"),
	creeps: require("manager.creeps"),
	sources: require("manager.sources"),
	roads: require("manager.roads"),
	construction: require("manager.construction"),
	visuals: require("manager.visuals")
};