const util = require("util");
const DistanceMap = require("prototype.distancemap");

module.exports =
{
	update: function()
	{
		if (Game.time % 5 != 0) return;

		const allSites = Memory.constructionSites || {};
		for (const roomName in allSites)
		{
			const roomSites = allSites[roomName];
			// Iterate back to front so we can delete finished sites
			for (let index = roomSites.length - 1; index >= 0; index--)
			{
				const site = roomSites[index];
				const pos = new RoomPosition(site.x, site.y, roomName);
				const foundSites = Game.rooms[roomName] ? pos.lookFor(LOOK_CONSTRUCTION_SITES) : undefined;
				// Apparently you only see a site 2 ticks after you create it, so to check if a site
				// has been built we need to have seen it once (has an id) or maybe we missed it? (some aomunt of ticks)
				if (foundSites && foundSites.length == 0 && (site.id || site.ticks > 20))
				{
					roomSites.splice(index, 1);
					if (roomSites.length == 0) delete Memory.constructionSites[roomName];
				}
				else
				{
					// Update existing sites
					if (foundSites && foundSites.length > 0)
					{
						if (!site.id)
						{
							// This is the first time we see it
							site.id = foundSites[0].id;
							site.progressTotal = foundSites[0].progressTotal;
						}
						site.progress = foundSites[0].progress;
						if (site.progress == 0 && site.type == STRUCTURE_ROAD)
						{
							if (!Memory.histogram[roomName] ||
								!Memory.histogram[roomName][site.y] ||
								!Memory.histogram[roomName][site.y][site.x] ||
								Memory.histogram[roomName][site.y][site.x] < util.ROAD_BUILD_THRESHOLD)
							{
								console.log("Removing site " + roomName + " " + site.x + "," + site.y);
								foundSites[0].remove();
							}
						}
					}
					site.ticks++;
				}
			}
		}
	},
	
	create: function(type, roomPosOrName, x, y)
	{
		const roomPos = (roomPosOrName instanceof RoomPosition) ? roomPosOrName : new RoomPosition(x, y, roomPosOrName);
		if (roomPos.x <= 0 || 49 <= roomPos.x || roomPos.y <= 0 || 49 <= roomPos.y) return;
		const roomSites = util.ensureArray("constructionSites", roomPos.roomName);
		roomSites.push({type: type, x: roomPos.x, y: roomPos.y, ticks: 1});
		const result = roomPos.createConstructionSite(type);
		if (result != OK) console.log("Could not build " + type + " at " + roomPos + ": " + util.errorString(result));
	},

	createAll: function(buildCount, roomName, occupied)
	{
		// Don't build everything at once.
		const roomSites = util.ensureArray("constructionSites", roomName);
		let createSites = 3 - roomSites.length;
		if (createSites > 0)
		{
			for (const type of util.BUILD_ORDER)
			{
				while (buildCount[type] > 0)
				{
					this.build(type, roomName, occupied);
					buildCount[type]--;
					createSites--;
					if (createSites <= 0) return;
				}
			}
		}
	},
	
	getSite: function(roomName, type)
	{
		if (!(roomName in Memory.constructionSites)) return;
		for (const siteInfo of Memory.constructionSites[roomName])
		{
			if (siteInfo.type == type) return new RoomPosition(siteInfo.x, siteInfo.y, roomName);
		}
		return false;
	},

	getSites: function(roomName)
	{
		const sites = Memory.constructionSites[roomName];
		return sites || [];
	},

	build: function(type, roomName, occupied)
	{
		const roomMem = Memory.rooms[roomName];
		const buildings = new DistanceMap(roomName, roomMem.buildings);
		for (const pos of util.goAround(new RoomPosition(roomMem.anchor.x, roomMem.anchor.y, roomName)))
		{
			if (buildings.get(pos) == type)
			{
				let free = true;
				for (const structure of occupied)
				{
					if (pos.isEqualTo(structure.pos))
					{
						if (type != structure.structureType || !structure.my)
						{
							console.log("Wanted " + type + " at " + pos + " and found " + structure.structureType);
							if (!structure.my) console.log("It wasn't mine");
							for (const structure of pos.lookFor(LOOK_STRUCTURES)) structure.destroy();
						}
						else
						{
							free = false;
							break;
						}
					}
				}
				if (free)
				{
					console.log("Build " + type + " at " + pos);
					this.create(type, pos);
					occupied.push({pos:pos, structureType:type});
					return;
				}
			}
		}
		console.log("Could not find a place for " + type + " in " + roomName);
	}
};