const specialisms = require("specialist._all");
const roomManager = require("manager.rooms");
const util = require("util");

module.exports =
{
	update: function()
	{
		if (!("monitor" in Memory.config) || !Game.creeps[Memory.config.monitor]) Memory.config.monitor = null;
		this.bringOutYourDead();

		const specialistCount = {};
		for (const specialism in specialisms) specialistCount[specialism] = 0;
		for (const name in Game.creeps)
		{
			const creep = Game.creeps[name];
			const specialism = creep.memory.specialism;
			creep.resetFuture();
			if (creep.ticksToLive <= 11)
			{
				creep.say(creep.ticksToLive - 1);
				creep.memory.old = true;
			}
			if (specialism) specialistCount[specialism]++;
			if (!creep.memory.home) creep.memory.home = creep.room.name;
			if (!creep.memory.body)
			{
				creep.memory.body = {};
				for (const part of BODYPARTS_ALL)
				{
					creep.memory.body[part] = _.filter(creep.body, {type:part}).length;
				}
			}
		}
		Memory.specialistCount = specialistCount;
		this.maintainPopulation();
	},
	
	lateUpdate: function()
	{
		for (const name in Game.creeps)
		{
			const creep = Game.creeps[name];
			if (creep.memory.lastPos && creep.pos.roomName != creep.memory.lastPos.roomName)
			{
				if (!creep.memory.lastPos.roomName) console.log("undef? " + JSON.stringify(creep.memory));
				roomManager.adjustDanger(creep.memory.lastPos.roomName, false);
			}
			if (!creep.spawning)
			{
				creep.memory.lastPos =
				{
					x: creep.pos.x,
					y: creep.pos.y,
					roomName: creep.pos.roomName
				};
			}
		}
	},

	bringOutYourDead: function()
	{
		for (const name in Memory.creeps)
		{
			const creep = Game.creeps[name];
			if (!creep)
			{
				console.log("† " + name);
				if (!Memory.creeps[name].old)
				{
					const lastPos = Memory.creeps[name].lastPos;
					if (lastPos) roomManager.adjustDanger(lastPos.roomName, true);
				}
				delete Memory.creeps[name];
			}
		}
	},

	creepNamesByHome: function()
	{
		// TODO: for now the home room is the room they happen to be in
		// This will be the room they were spawned in or the room they should relocate to
		const homeToCreepname = {};
		for (const creepName in Game.creeps)
		{
			if (Memory.creeps[creepName].specialism) continue;

			const roomName = Game.creeps[creepName].memory.home;
			if (!homeToCreepname[roomName]) homeToCreepname[roomName] = [];
			homeToCreepname[roomName].push(creepName);
		}
		return homeToCreepname;
	},

	maintainPopulation: function()
	{
		const workersPerHome = this.creepNamesByHome();
		const minimumAmount = 3;
		for (const roomName in Memory.rooms.owned)
		{
			const room = Game.rooms[roomName];
			const creepsForThisRoom = workersPerHome[roomName];
			const workerAmount = creepsForThisRoom ? creepsForThisRoom.length : 0;
			const wantedAmount = room.memory.targetCreepAmount || minimumAmount;
			if (workerAmount < wantedAmount)
			{
				let parts;
				// If there are less than the minimum amount of workers it can be because:
				// - we are starting up: spawn only creeps for 300 which we can get for free
				// - we are doing well and want only a couple of creeps: spawn normally
				// I'm lazy and don't really know which is the case, so we'll spawn what we can afford
				// Either way, if there are enough creeps to fill the extensions then we always save up
				let budget = Math.max(300, workerAmount < minimumAmount ? room.energyAvailable : room.energyCapacityAvailable);
				if (budget < 350) parts = [CARRY, MOVE, CARRY, WORK, MOVE];
				else
				{
					const partOrder = [MOVE, WORK, CARRY];
					let nextPartIndex = 0;
					parts = [];
					while (parts.length < 39 /*MAX_CREEP_SIZE*/ && BODYPART_COST[partOrder[nextPartIndex]] <= budget)
					{
						parts.push(partOrder[nextPartIndex]);
						budget -= BODYPART_COST[partOrder[nextPartIndex]];
						nextPartIndex = (nextPartIndex + 1) % partOrder.length;
					}
				}
				util.spawn(parts, roomName);
			}
		}
	}
};