const util = require("util");
const conMan = require("manager.construction");
const roomMan = require("manager.rooms");
const MAX = 35;
const SUPPRESS = 0.1;
const DECAY = 0.01;

module.exports =
{
	update: function()
	{
		for (const name in Game.creeps)
		{
			const creep = Game.creeps[name];
			if (creep.memory.lastPos && !util.sameRoomPos(creep.memory.lastPos, creep.pos))
			{
				util.ensure("histogram", creep.pos.roomName, creep.pos.y);
				const rowHist = Memory.histogram[creep.pos.roomName][creep.pos.y];
				const isSwamp = creep.pos.lookFor(LOOK_TERRAIN)[0] == "swamp";
				rowHist[creep.pos.x] = Math.min(MAX, (rowHist[creep.pos.x] || 0) + (isSwamp ? 3.6 : 1.2));
				util.around(creep.pos).forEach(function(pos)
				{
					if (Memory.histogram[pos.roomName][pos.y] &&
						Memory.histogram[pos.roomName][pos.y][pos.x])
					{
						Memory.histogram[pos.roomName][pos.y][pos.x] -= SUPPRESS;
					}
				});
				if (rowHist[creep.pos.x] > util.ROAD_BUILD_THRESHOLD &&
					creep.pos.lookFor(LOOK_STRUCTURES).length == 0 &&
					roomMan.isFree(creep.pos))
				{
					conMan.create(STRUCTURE_ROAD, creep.pos);
				}
			}
		}
		
		// Delete empty parts
		for (const roomName in Memory.histogram)
		{
			const roomHist = Memory.histogram[roomName];
			let roomEmpty = true;
			for (const row in roomHist)
			{
				const rowHist = roomHist[row];
				let rowEmpty = true;
				for (const col in rowHist)
				{
					rowHist[col] -= DECAY;
					if (rowHist[col] > 0) rowEmpty = false;
					else delete rowHist[col];
				}
				if (rowEmpty) delete roomHist[row];
				else roomEmpty = false;
			}
			if (roomEmpty) delete Memory.histogram[roomName];
		}
		
		if (Memory.showHistogram) this.showHistogram();
		else if (Memory.isShowingHistogram)
		{
			util.clearFlags();
			Memory.isShowingHistogram = false;
		}
	},
	
	importance: function(roomPos)
	{
		return ((util.valueIfExists("histogram", roomPos.roomName, roomPos.y, roomPos.x) || 0) - util.ROAD_BUILD_THRESHOLD) / (MAX - util.ROAD_BUILD_THRESHOLD);
	},
	
	showHistogram: function()
	{
		for (const roomName in Memory.histogram)
		{
			const room = Game.rooms[roomName];
			const roomHist = Memory.histogram[roomName];
			for (const row in roomHist)
			{
				const rowHist = roomHist[row];
				for (const col in rowHist)
				{
					util.makeFlag(new RoomPosition(+col, +row, roomName), rowHist[col] / util.ROAD_BUILD_THRESHOLD);
				}
			}
		}
		Memory.isShowingHistogram = true;
	},

	moveCautious: function(roomName, matrix)
	{
		if (Memory.rooms.owned[roomName]) return;
		const room = Game.rooms[roomName];
		if (!room)
		{
			const intel = util.valueIfExists("rooms", "seen", roomName);
			if (intel)
			{
				if (intel.hasTowers)
				{
					for (let index = 0; index < util.roomSize.width; index++)
					{
						matrix.set(index,  0, 100);
						matrix.set(index, 49, 100);
						matrix.set( 0, index, 100);
						matrix.set(49, index, 100);
					}
				}
			}
			return;
		}
		for (const enemy of room.find(FIND_HOSTILE_CREEPS))
		{
			for (const pos of util.goAround(enemy.pos, 3))
			{
				matrix.set(pos.x, pos.y, 20);
			}
		}
	}
};