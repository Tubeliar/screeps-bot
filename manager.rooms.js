// Keep information on any room that we have any business in

const util = require("util");
const conMan = require("manager.construction");
const sourceMan = require("manager.sources");
const visuals = require("manager.visuals");
const DistanceMap = require("prototype.distancemap");
const DANGER_ALPHA = 0.1;

const assign = function(assignments, pos, structure, buildings)
{
	assignments.set(pos, structure);
	buildings.remove(pos);
};

module.exports =
{
	update: function()
	{
		util.ensure("rooms.seen");
		util.ensure("rooms.owned");
		util.ensure("rooms.reserved");
		
		this.spyOnRooms();

		if (Game.time % 5 != 1) return;
		
		// Maintain owned rooms
		for (const roomName in Memory.rooms.owned)
		{
			// If we can't see a room we must have lost it
			const room = Game.rooms[roomName];
			if (!room)
			{
				console.log("Lost room " + roomName);
				delete Memory.rooms.owned[roomName];
				delete Memory.rooms[roomName];
				delete Memory.sources[roomName];
				continue;
			}
			// Make sure there is a plan for this room. Newly claimed rooms will have no plan and no spawn.
			// In the very first room the spawn is already placed, but we still need to reserve spots
			const spawns = room.find(FIND_MY_STRUCTURES, { filter: { structureType: STRUCTURE_SPAWN } });
			if (!room.memory.buildings)
			{
				const spawnPos = spawns.length == 0 ? conMan.getSite(roomName, STRUCTURE_SPAWN) : spawns[0].pos;
				if (spawnPos) this.initRoom(spawnPos);
				else this.initRoom(roomName);
			}
			
			// Build things
			if (room.controller)
			{
				// See how much of everything we are allowed. Then subtract how much we already have.
				const buildCount = {};
				const structures = room.find(FIND_MY_STRUCTURES);
				// Since we have a complete list of structures now, let's verify that they are in compliance with the plan
				if (room.memory.buildings)
				{
					const buildings = new DistanceMap(roomName, room.memory.buildings);
					for (const structure of structures)
					{
						if (structure.structureType == STRUCTURE_CONTROLLER) continue;
						if (buildings.get(structure.pos) != structure.structureType)
						{
							if (structure.structureType == STRUCTURE_SPAWN && room.controller.level < 2)
							{
								console.log("Room " + room.name + " needs to be reinitialized");
								delete Memory.rooms[room.name];
								// We actually need to break two levels, but there's going to be a lot of work the next tick, so finish early
								return;
							}
							else
							{
								const result = structure.destroy();
								console.log("Destroying " + structure + " " + structure.pos + ": " + util.errorString(result));
							}
						}
					}
				}
				const constructionSites = room.find(FIND_MY_CONSTRUCTION_SITES);
				for (const type of util.BUILD_ALL) buildCount[type] = CONTROLLER_STRUCTURES[type][room.controller.level];
				for (const type of util.BUILD_ONE) buildCount[type] = Math.min(1, CONTROLLER_STRUCTURES[type][room.controller.level]);
				// Exclude the controller by checking the key is in buildCount
				for (const struct of structures) if (struct.structureType in buildCount) buildCount[struct.structureType]--;
				for (const struct of constructionSites) buildCount[struct.structureType]--;
				conMan.createAll(buildCount, roomName, structures.concat(constructionSites));
			}
		}
		
		// Set expansion target
		let readyToExpand = _.size(Memory.rooms.owned) < Game.gcl.level;
		if (readyToExpand && !Memory.expandTo)
		{
			for (const roomName in Memory.sources)
			{
				for (const id in Memory.sources[roomName])
				{
					readyToExpand &= Memory.sources[roomName][id].depletion > 0.9;
				}
			}
			if (readyToExpand)
			{
				const newRoom = this.findNewRoom();
				if (newRoom.score > -Infinity) Memory.expandTo = {roomName: newRoom.roomName, reserve: false};
			}
		}
	},
	
	spyOnRooms: function()
	{
		// Spy on every room we can see
		for (const roomName in Game.rooms)
		{
			const room = Game.rooms[roomName];
			// Check if the room changed ownership
			if (room.controller)
			{
				if (room.controller.my)
				{
					Memory.rooms.owned[roomName] = true;
					delete Memory.rooms.seen[roomName];
					delete Memory.rooms.reserved[roomName];
					continue;
				}
				else if (Memory.rooms.owned[roomName])
				{
					delete Memory.rooms.owned[roomName];
				}
			}
			if (Memory.rooms.owned[roomName] || Memory.rooms.reserved[roomName]) continue;

			this.spyOnRoom(room);
		}
	},

	spyOnRoom: function(room)
	{
		// Store or update whatever we can learn about this room
		util.ensure("rooms", "seen", room.name);
		const roomMem = Memory.rooms.seen[room.name];
		const sources = room.find(FIND_SOURCES);
		roomMem.hasTowers = room.find(FIND_STRUCTURES, {filter: {structureType: STRUCTURE_TOWER}}).length != 0;
		if (room.controller)
		{
			roomMem.controller = {x: room.controller.pos.x, y: room.controller.pos.y};
			if (room.controller.owner && !room.controller.my) roomMem.controller.owned = room.controller.owner.username;
			if (room.controller.reservation) roomMem.controller.reserved = room.controller.reservation.username;
			roomMem.controller.distanceToSource = [];
			for (const source of sources)
			{
				roomMem.controller.distanceToSource.push(room.controller.pos.findPathTo(source).length);
			}
		}
		roomMem.sources = {};
		for (const source of sources)
		{
			roomMem.sources[source.id] = {x: source.pos.x, y: source.pos.y};
		}
		roomMem.minerals = {};
		for (const mineral of room.find(FIND_MINERALS))
		{
			roomMem.minerals[mineral.id] = {type: mineral.mineralType, density: mineral.density, x: mineral.pos.x, y: mineral.pos.y};
		}
		if (!roomMem.map)
		{
			const AreaMap = require("prototype.areamap");
			roomMem.map = new AreaMap(room.name).data;
		}
	},

	adjustDanger: function(roomName, dangerous)
	{
		const roomMem = Memory.rooms.seen[roomName];
		if (!roomName || !roomMem) return;
		const danger = roomMem.danger;
		const adjustment = dangerous ? 1 : 0;
		roomMem.danger = (1 - DANGER_ALPHA) * (danger ? danger : 0) + DANGER_ALPHA * adjustment;
	},
	
	initRoom: function(where)
	{
		console.log("init "+ where);
		const roomName = (where instanceof RoomPosition) ? where.roomName : where;
		// If we want to replan a room we already own then put the relevant data in 'seen' for now
		if (roomName in Memory.rooms.owned) this.spyOnRoom(Game.rooms[roomName]);
		const plan = this.plan(where, false);
		Memory.rooms[roomName].buildings = plan.buildings.data;
		Memory.rooms[roomName].borders = plan.borders;
		Memory.rooms[roomName].anchor = plan.anchor;
	},

	plan: function(where, show = true)
	{
		let spawnLocation = undefined;
		let roomName = where;
		if (where instanceof RoomPosition)
		{
			spawnLocation = where;
			roomName = where.roomName;
		}
		const RoomPlanner = require("prototype.roomplanner");
		const plan = new RoomPlanner(where).plan();
		const assignments = new DistanceMap(roomName);

		if (spawnLocation) assign(assignments, spawnLocation, STRUCTURE_SPAWN, plan.buildings);

		// TOWERS
		// find which parts of the fringe don't need to be defended because they end up in caves.
		// We can't use the exitmap values for this because a base that is close to one exit could have a
		// gradient going across the whole base erroneously thinking the back perimeter is sealed.
		const regionMap = plan.exitMap.copy();
		for (const point of plan.reach.positions()) regionMap.remove(point);
		const regions = regionMap.connectedComponents();
		const exitSeeds = DistanceMap.exitSeeds(roomName);
		for (const region of regions)
		{
			let hasExits = false;
			for (const point of exitSeeds.positions())
			{
				if (region.contains(point))
				{
					hasExits = true;
					break;
				}
			}
			region.hasExits = hasExits;
		}
		const openReachParts = [];
		for (const fringePart of plan.fringe.connectedComponents())
		{
			// We only need to sample one point to know if this border needs to be protected
			const sample = fringePart.positions().next().value;
			let needsDefending = false;
			for (const region of regions) needsDefending |= region.contains(sample) && region.hasExits;
			if (!needsDefending) continue;

			const reachPart = new DistanceMap(roomName);
			for (const point of fringePart.positions())
			{
				for (const neighbour of util.around(point))
				{
					if (plan.reach.contains(neighbour)) reachPart.set(neighbour, true);
				}
			}
			openReachParts.push(reachPart);
		}
		// Each border must be assigned at least one tower building spot
		const perimeter = new DistanceMap(roomName);
		for (const openReachPart of openReachParts)
		{
			for (const point of openReachPart.positions())
			{
				for (const neighbour of util.around(point))
				{
					if (plan.buildings.contains(neighbour)) perimeter.set(neighbour, true);
				}
			}
			if (perimeter.isEmpty()) perimeter.set(plan.buildings.nearest(openReachPart.average()), true);
		}

		// Being far away from exits is a penalty for a tower spot
		const towerSpots = new DistanceMap(roomName);
		for (const spot of perimeter.positions())
		{
			towerSpots.set(spot, -plan.exitMap.get(spot));
		}
		for (let towersPlaced = 0; towersPlaced < CONTROLLER_STRUCTURES[STRUCTURE_TOWER][8]; towersPlaced++)
		{
			let bestSpot = towerSpots.reduce((comp, current) => current > comp).positions[0];
			// If the perimeter is very small (for instance because the whole base fits in a cave) we can run out of spots
			if (!bestSpot) bestSpot = plan.buildings.nearest(perimeter.average());
			towerSpots.remove(bestSpot);
			assign(assignments, bestSpot, STRUCTURE_TOWER, plan.buildings);
			for (const posVal of towerSpots)
			{
				// Being far away from existing towers is good, so add the distance to the chosen spot to the others' scores
				towerSpots.set(posVal.roomPos, posVal.value + util.euclideanDistance(bestSpot, posVal.roomPos));
			}
		}

		// LABS
		// We're looking for a compact block. Find a 3x3 block that has as many building spots in it as possible.
		// Break ties with open spots being close to exits, since boosteds creeps are always going somewhere.
		const labSpots = new DistanceMap(roomName);
		for (const point of plan.buildings.positions())
		{
			let score = 0;
			let dist = Infinity;
			for (const inBlock of util.growBlock(point, 3))
			{
				if (plan.buildings.contains(inBlock)) score++;
				else if (plan.exitMap.contains(inBlock)) dist = Math.min(dist, plan.exitMap.get(inBlock));
			}
			labSpots.set(point, {score: score, dist: dist});
		}
		const labCandidates = labSpots.reduce((comp, current) => current.score > comp.score || (current.score == comp.score && current.dist < comp.dist));
		// The returned position is the topleft corner of a 3x3 block, so add 1,1 to getvthe center
		const labAnchor = util.relativePosition(labCandidates.positions[0], 1, 1);
		let labsPlaced = 0;
		for (const point of util.goAround(labAnchor))
		{
			if (!plan.buildings.contains(point)) continue;
			assign(assignments, point, STRUCTURE_LAB, plan.buildings);
			labsPlaced++;
			if (labsPlaced >= CONTROLLER_STRUCTURES[STRUCTURE_LAB][8]) break;
		}

		// The rest of the structures just need to be somewhere cental on the base, either because we need easy access to them
		// or because we value them more than extensions and want to protect them.
		const placeIterator = plan.buildings.goAround(plan.anchor);
		assign(assignments, placeIterator.next().value, STRUCTURE_STORAGE, plan.buildings);
		assign(assignments, placeIterator.next().value, STRUCTURE_TERMINAL, plan.buildings);
		assign(assignments, placeIterator.next().value, STRUCTURE_LINK, plan.buildings);
		assign(assignments, placeIterator.next().value, STRUCTURE_POWER_SPAWN, plan.buildings);
		assign(assignments, placeIterator.next().value, STRUCTURE_SPAWN, plan.buildings);
		assign(assignments, placeIterator.next().value, STRUCTURE_SPAWN, plan.buildings);
		if (!spawnLocation) assign(assignments, placeIterator.next().value, STRUCTURE_SPAWN, plan.buildings);
		assign(assignments, placeIterator.next().value, STRUCTURE_NUKER, plan.buildings);
		assign(assignments, placeIterator.next().value, STRUCTURE_OBSERVER, plan.buildings);
		for (const pos of plan.buildings.positions())
		{
			assignments.set(pos, STRUCTURE_EXTENSION);
		}
		const result = {buildings: assignments, borders: openReachParts, anchor: plan.anchor};
		
		if (!show) return result;

		delete Memory.visuals;
		const circles = util.ensureArray("visuals", roomName, "circles");
		visuals.buildings(assignments, roomName);
		for (const point of plan.reach.positions())
		{
			circles.push({ x: point.x, y: point.y, style: { radius: 0.5, fill: "#000", opacity: 0.2 } });
		}
		for (const openReachPart of openReachParts)
		{
			for (const point of openReachPart.positions())
			{
				circles.push({ x: point.x, y: point.y, style: { radius: 0.05, fill: "#F00", opacity: 1 } });
			}
		}
		return result;
	},
	
	isFree: function(roomPos)
	{
		if (util.valueIfExists("rooms", roomPos.roomName, "buildings", roomPos.y, roomPos.x) ||
			roomPos.lookFor(LOOK_TERRAIN)[0] == "wall" ||
			roomPos.lookFor(LOOK_CONSTRUCTION_SITES).length > 0)
		{
			return false;
		}
		const structures = roomPos.lookFor(LOOK_STRUCTURES);
		return structures.length == 0 || structures.length == 1 && structures[0].structureType == STRUCTURE_ROAD;
	},
	
	findNewRoom: function(spawn)
	{
		let best = {roomName: null, score: -Infinity, sources: 0};
		for (const roomName in Memory.rooms.seen)
		{
			const result = this.scoreRoom(roomName, spawn);
			const score = result.score;
			const nSources = result.nSources;
			if (spawn ? score > best.score : nSources == best.sources && score > best.score || nSources > best.sources)
			{
				best = {roomName: roomName, score: score, sources: nSources};
			}
		}
		return best;
	},

	scoreRoom: function(roomName, spawn)
	{
		const noResult = {"score": 0, "nSources": 0};
		const roomMem = Memory.rooms.seen[roomName];
		if (!roomMem) return noResult;
		
		if (!roomMem.controller || roomMem.controller.owned || roomMem.controller.reserved) return noResult;
		
		const sourceKeys = Object.keys(roomMem.sources);
		const nSources = sourceKeys.length;
		if (nSources <= 0) return noResult;
		
		let interDist = _.min(roomMem.controller.distanceToSource);
		if (nSources == 2)
		{
			const source1 = roomMem.sources[sourceKeys[0]];
			const source2 = roomMem.sources[sourceKeys[1]];
			const sourceToSource = (new RoomPosition(source1.x, source1.y, roomName)).getRangeTo(source2.x, source2.y);
			interDist += Math.min(_.max(roomMem.controller.distanceToSource), sourceToSource);
		}
		else
		{
			// TODO: evaluate rooms with 3 sources
			interDist = _.sum(roomMem.controller.distanceToSource);
		}
		let transferDist = 0;
		if (spawn)
		{
			// Get the distances from the spawn to each important object in this room
			const spawnToRc = PathFinder.search(spawn.pos, new RoomPosition(roomMem.controller.x, roomMem.controller.y, roomName));
			if (spawnToRc.path[spawnToRc.path.length - 1].roomName != roomName) return noResult;
		
			const sourceDistances = [];
			for (const source of roomMem.sources)
			{
				const sd = PathFinder.search(spawn.pos, new RoomPosition(source.x, source.y, roomName));
				sourceDistances.push(sd.cost);
			}
			// Get the distance to the closest important object and use it as an indication of how hard it is to reach this room
			transferDist = Math.min(_.min(sourceDistances), spawnToRc.cost);
		}
		return {"score": (interDist + transferDist) / -nSources, "nSources": nSources};
	},

	summary: function()
	{
		console.log("Room  lvl  next lvl  prgrs    amt  sts  depletion");
		for (const roomName in Memory.rooms.owned)
		{
			const room = Game.rooms[roomName];
			const roomMem = room.memory;
			const rc = room.controller;
			let message = 
				util.width(roomName, 6) +
				util.width(rc.level, 3) + "  " +
				util.width(rc.progressTotal - rc.progress, 8) + "  " +
				util.width(100 * rc.progress / rc.progressTotal, 5) + "  " +
				util.width(roomMem.targetCreepAmount + roomMem.employment, 5) + "  " +
				util.width(conMan.getSites(roomName).length, 3)
			;
			const sources = sourceMan.getSources(roomName);
			for (const sourceId in sources)
			{
				message += "  " + util.width(sources[sourceId].depletion, 4);
			}
			console.log(message);
		}
	}
};