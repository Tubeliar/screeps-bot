
const util = require("util");

const depletionAlpha = 0.1;

module.exports =
{
	update: function()
	{
		if (Game.time % 5 != 2) return;

		util.ensure("sources");
		const allMyRooms = Object.assign({}, Memory.rooms.owned, Memory.rooms.reserved);
		for (const roomName in allMyRooms)
		{
			const room = Game.rooms[roomName];
			if (!Memory.sources[roomName]) Memory.sources[roomName] = {};
			
			room.find(FIND_SOURCES).forEach(function(source)
			{
				if (Memory.sources[roomName][source.id])
				{
					// Maybe check the surroundings again, for blocking structures?
					let energy = Memory.sources[roomName][source.id].energy;
					if (energy == undefined) energy = 0;
					let depletion = Memory.sources[roomName][source.id].depletion;
					if (depletion == undefined) depletion = 0.5;

					if (energy > 0)
					{
						if (source.energy == 0)
						{
							// This is the tick where the source was depleted
							const tooFast = 1 - source.ticksToRegeneration / ENERGY_REGEN_TIME;
							Memory.sources[roomName][source.id].depletion = depletionAlpha * (1 / tooFast) + (1 - depletionAlpha) * depletion;
						}
						else if (source.energy > energy)
						{
							// This is the tick where the source regenerated before we depleted it
							Memory.sources[roomName][source.id].depletion = depletionAlpha * (1 - energy / source.energyCapacity) + (1 - depletionAlpha) * depletion;
						}
					}
					Memory.sources[roomName][source.id].energy = source.energy;
				}
				else
				{
					const sourceInfo = Memory.sources[roomName][source.id] = {};
					sourceInfo.pos = source.pos;
					sourceInfo.harvestPositions = [];
					util.around(source.pos).forEach(function(neighbour)
					{
						if (!_.includes(room.lookForAt(LOOK_TERRAIN, neighbour), "wall"))
						{
							sourceInfo.harvestPositions.push(neighbour);
						}
					});
				}
			});
		}
	},

	getSources: function(roomName)
	{
		return Memory.sources[roomName];
	},

	getOneSource: function(roomName)
	{
		let oneSource = null;
		for (const id in Memory.sources[roomName])
		{
			const source = Memory.sources[roomName][id];
			if (!oneSource || source.depletion < oneSource.depletion) oneSource = source;
		}
		return oneSource;
	}
};