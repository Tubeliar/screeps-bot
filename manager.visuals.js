const util = require("util");
const DistanceMap = require("prototype.distancemap");

module.exports =
{
	update: function()
	{
		for (const roomName in Memory.visuals)
		{
			const roomVisuals = Memory.visuals[roomName];
			const draw = new RoomVisual(roomName);
			if (roomVisuals.circles) roomVisuals.circles.forEach(function(command){draw.circle(+command.x, +command.y, command.style);});
			if (roomVisuals.polys) for (let i = 0; i < roomVisuals.polys.length; i++) this.poly(draw, roomVisuals.polys[i]);
		}
	},

	poly: function(draw, command)
	{
		let points = [];
		command.points.forEach(function(point)
		{
			points.push([+point.x, +point.y]);
		});
		draw.poly(points, command.style);
	},

	distanceMap: function(distanceMap, roomName)
	{
		let circles = util.ensureArray("visuals", roomName, "circles");
		for (const row in distanceMap)
		{
			for (const col in distanceMap[row])
			{
				circles.push({x: col, y: row, style: {radius: 0.12, fill: util.hue(distanceMap[row][col] / 60)}});
			}
		}
	},

	plan: function(roomName)
	{
		const buildings = new DistanceMap(roomName, Memory.rooms[roomName].buildings);
		this.buildings(buildings, roomName);
	},

	buildings: function(assignments, roomName)
	{
		const circles = util.ensureArray("visuals", roomName, "circles");
		for (const posVal of assignments)
		{
			let style = { radius: 0.35, fill: "#FF0" };
			switch (posVal.value)
			{
			case STRUCTURE_NUKER: style.radius = 0.5;
			case STRUCTURE_TOWER: style.fill = "#F00"; break;

			case STRUCTURE_TERMINAL: style.radius = 0.45;
			case STRUCTURE_LINK: style.fill = "#aaa"; break;

			case STRUCTURE_POWER_SPAWN: style.radius = 0.45;
			case STRUCTURE_SPAWN: style.fill = "#0FF"; break;

			case STRUCTURE_LAB: style.fill = "#FFF"; break;
			case STRUCTURE_EXTENSION: style.fill = "#880"; break;
			case STRUCTURE_STORAGE: style.radius = 0.45; break;
			case STRUCTURE_OBSERVER: style = { radius: 0.25, fill: "#F08" }; break;
			}
			circles.push({ x: posVal.roomPos.x, y: posVal.roomPos.y, style: style });
		}
	}
};