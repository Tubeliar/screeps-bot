const taskTypes = require("task._all");
const creepManager = require("manager.creeps");
const sourceMan = require("manager.sources");
const util = require("util");

module.exports =
{
	getPlans: function()
	{
		const creepsByHome = creepManager.creepNamesByHome();
		const masterPlan = {};
		const leftCreeps = [];
		const leftTasks = [];
		for (const roomName in Memory.rooms.owned)
		{
			const creepNames = creepsByHome[roomName];
			// It is possible that there are no creeps that belong to this room, either if the room was invaded or newly founded.
			if (!creepNames) continue;
			this.addRoomPlans(roomName, creepNames, masterPlan, leftCreeps, leftTasks);
		}
		for (const roomName in Memory.rooms.reserved)
		{
			this.addRoomPlans(roomName, creepsByHome[roomName], masterPlan, leftCreeps, leftTasks);
		}
		// TODO: make plans for rooms we neither own nor have reserved.
		// Especially opportunities in transit like grabbing resources or harvesting underway

		// TODO: See if we can't have leftover creeps do leftover tasks
		for (const name of leftCreeps)
		{
			const creep = Game.creeps[name];
			const sourceInfo = sourceMan.getOneSource(creep.memory.home);
			if (creep.pos.getRangeTo(sourceInfo.pos.x, sourceInfo.pos.y) <= 2) continue;
			creep.moveTo(new RoomPosition(sourceInfo.pos.x, sourceInfo.pos.y, sourceInfo.pos.roomName), { ignoreCreeps: true, range: 1 });
		}

		return masterPlan;
	},

	addRoomPlans: function(roomName, creepNames, plans, leftCreeps, leftTasks)
	{
		const tasks = this.getTasks(roomName);
		const fitnessMatrix = this.createMatrix(tasks, creepNames);
		const workerCount = _.size(fitnessMatrix);
		
		let assignedCount = 0;
		let showMatrix = Memory.rooms[roomName].showMatrix;
		if (showMatrix > 0) console.log("Plans for " + roomName);
		while (!_.isEmpty(fitnessMatrix))
		{
			if (showMatrix > 0) this.log(tasks, fitnessMatrix);
			const assignment = this.assignOne(fitnessMatrix);
			if (!assignment) break;
			assignedCount++;
			const creep = Game.creeps[assignment.name];
			const task = tasks[assignment.index];
			if (showMatrix > 0) console.log(creep.name + " will do " + assignment.index + ": " + JSON.stringify(task));
			plans[creep.name] = task;
			
			const replacement = this.updateMatrix(tasks, fitnessMatrix, assignment);
			if (showMatrix > 0)
			{
				console.log("Replacement task: " + JSON.stringify(replacement));
				console.log("-------");
				showMatrix--;
			}
		}
		delete Memory.rooms[roomName].showMatrix;
		
		// Any creeps left out have their previous task erased
		// TODO: Maybe only do this if the creep's task was in this room.
		// Even though the creep belongs in this room he might have gone out for an errand.
		for (const name in fitnessMatrix)
		{
			delete Game.creeps[name].memory.currentTask;
			leftCreeps.push(name);
		}

		const tasksNotDone = _.filter(tasks, task => !task.done);
		leftTasks.push.apply(leftTasks, tasksNotDone);

		this.adjustCreepAmount(roomName, assignedCount, workerCount, tasksNotDone.length);
	},

	getTasks: function(roomName)
	{
		const tasks = [];
		for (const type in taskTypes)
		{
			tasks.push.apply(tasks, taskTypes[type].getTasks(roomName));
		}
		return tasks;
	},
	
	/**
	 * Generate a fitness matrix for the given tasks, only considering the given creeps.
	 * The resulting matrix is an object with creep names as keys and arrays with tasks
	 * fitness scores as values.
	 */
	createMatrix: function(tasks, creepNames)
	{
		const fitnessMatrix = {};
		creepNames.forEach(function(name)
		{
			const creep = Game.creeps[name];
			if (creep.spawning) return;
			if (creep.memory.specialism) return;
			
			fitnessMatrix[name] = [];
			for (let index = 0; index < tasks.length; index++)
			{
				fitnessMatrix[name].push(tasks[index].getFitness(creep));
			}
		});
		return fitnessMatrix;
	},
	
	updateMatrix: function(tasks, fitnessMatrix, assignment)
	{
		const assignedTask = tasks[assignment.index];
		const assignedCreep = Game.creeps[assignment.name];
		
		// Delete the row for the assigned creep
		delete fitnessMatrix[assignedCreep.name];
		
		// Update the tasks list
		const replacement = assignedTask.adjust(assignedCreep);
		if (replacement) tasks[assignment.index] = replacement;
		else tasks.splice(assignment.index, 1);
		
		// Delete or update the row for the assigned task
		for (const name in fitnessMatrix)
		{
			if (replacement) fitnessMatrix[name][assignment.index] = replacement.getFitness(Game.creeps[name]);
			else fitnessMatrix[name].splice(assignment.index, 1);
		}
		
		return replacement;
	},
	
	assignOne: function(fitnessMatrix)
	{
		// find best match
		const match = {score: Number.NEGATIVE_INFINITY};
		for (const name in fitnessMatrix)
		{
			const scores = fitnessMatrix[name];
			for (let index = 0; index < scores.length; index++)
			{
				if (scores[index] > match.score)
				{
					match.name = name;
					match.index = index;
					match.score = scores[index];
				}
			}
		}
		if (match.score <= 0) return null;
		return {name: match.name, index: match.index};
	},
	
	log: function(tasks, fitnessMatrix)
	{
		let header = "Name        Carry  x  y  doing   ";
		for (let index = 0; index < tasks.length; index++)
		{
			header += " " + util.width(index, 5);
			console.log("[" + index + "] " + JSON.stringify(tasks[index]));
		}
		console.log(header);
		for (const name in fitnessMatrix)
		{
			const creep = Game.creeps[name];
			const cTask = creep.memory.currentTask;
			let line = util.width(name, 12) +
				" "  + util.width(_.sum(creep.carry), 3) +
				" (" + util.width(creep.pos.x, 2) +
				","  + util.width(creep.pos.y, 2) +
				") " + util.width(cTask ? cTask.class : "-", 8);
			const fitRow = fitnessMatrix[name];
			for (let index = 0; index < fitRow.length; index++)
			{
				line += " " + util.width(fitRow[index], 5);
			}
			console.log(line);
		}
	},
	
	adjustCreepAmount: function(roomName, assignedCount, workerCount, tasksNotDone)
	{
		const roomMem = Memory.rooms[roomName];
		let wantedAmount = roomMem.targetCreepAmount;
		// Newly claimed rooms will want a lot of small creeps to get them started
		if (!wantedAmount) wantedAmount = roomMem.targetCreepAmount = 10;
		
		let adjustEmployment = assignedCount - workerCount; // Negative if there were creeps unassigned, at most 0
		if (adjustEmployment >= 0) adjustEmployment = Math.min(tasksNotDone, workerCount); // Positive if there were tasks unassigned
		if (roomMem.showEmployment)
		{
			console.log(roomName + " Workers: " + workerCount + " assigned: " + assignedCount + " not done: " + tasksNotDone +
				" dec? " + (workerCount > assignedCount) + " inc? " + (tasksNotDone > 0) + "  wanted amount: " + wantedAmount);
		}
		adjustEmployment /= CREEP_LIFE_TIME;
		const employment = roomMem.employment;
		if (adjustEmployment > 0 && workerCount >= roomMem.targetCreepAmount ||
			adjustEmployment < 0 && workerCount <= roomMem.targetCreepAmount  )
		{
			roomMem.employment = (employment === undefined || isNaN(employment) ? 0.5 : employment) + adjustEmployment;
		}
		if (roomMem.employment < 0)
		{
			// There are creeps that do nothing. We have to lower the amount of creeps
			roomMem.targetCreepAmount--;
			roomMem.employment += 1;
		}
		else if (Memory.rooms[roomName].employment >= 1)
		{
			// There are tasks left undone. We need more creeps.
			roomMem.targetCreepAmount++;
			roomMem.employment -= 1;
		}
		if (Memory.config.trackEmployment)
		{
			if (!roomMem.employmentHistory) roomMem.employmentHistory = [];
			roomMem.employmentHistory.push(roomMem.employment);
			if (roomMem.employmentHistory.length > CREEP_LIFE_TIME * 2) roomMem.employmentHistory.shift();
			
			if (roomMem.showEmployment) util.logGraph(roomMem.employmentHistory);
		}
		else
		{
			delete roomMem.employmentHistory;
			if (roomMem.showEmployment) console.log("Can't show employment for " + roomName + ". Set Memory.config.trackEmployment to true first.");
		}
	}
};