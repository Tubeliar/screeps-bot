const util = require("util");

// An AreaMap stores the layout of a room (plain/swamp/wall) so that you can reason about it
// even when you don't have vision into the room.
var AreaMap = function(roomName, data)
{
	this.roomName = roomName;
	if (data) this.data = data;
	else
	{
		const room = Game.rooms[roomName];
		const terrain = room.lookForAtArea(LOOK_TERRAIN, 0, 0, util.roomSize.width - 1, util.roomSize.height - 1);
		const map = [];
		for (const rowNr in terrain)
		{
			const row = [];
			for (const colNr in terrain[rowNr])
			{
				if (terrain[rowNr][colNr] == "wall") row[colNr] = "#";
				else if (terrain[rowNr][colNr] == "swamp") row[colNr] = ".";
				else row[colNr] = " ";
			}
			map[rowNr] = row.join("");
		}
		this.data = map.join("\n");
	}
};

AreaMap.prototype.get = function(pos)
{
	if (typeof pos.x !== "number") pos.x = parseInt(pos.x);
	if (typeof pos.y !== "number") pos.y = parseInt(pos.y);
	return this.data[pos.y * (util.roomSize.width + 1) + pos.x];
};

AreaMap.prototype.isWall = function(pos)
{
	return this.get(pos) == "#";
};

AreaMap.prototype.isSwamp = function (pos)
{
	return this.get(pos) == ".";
};

AreaMap.prototype.isPlain = function (pos)
{
	return this.get(pos) == " ";
};

module.exports = AreaMap;