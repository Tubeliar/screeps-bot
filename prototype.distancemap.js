const util = require("util");

var DistanceMap = function(roomName, data)
{
	this.roomName = roomName;
	this.data = (typeof data === "object") ? data : {};
};

DistanceMap.prototype.size = function()
{
	let count = 0;
	for (let rowNr in this.data)
	{
		count += Object.keys(this.data[rowNr]).length;
	}
	return count;
};

DistanceMap.prototype.isEmpty = function()
{
	return _.isEmpty(this.data);
};

DistanceMap.prototype.set = function(pos, value)
{
	if (!this.data[pos.y]) this.data[pos.y] = {};
	this.data[pos.y][pos.x] = value;
};

DistanceMap.prototype.contains = function(pos)
{
	if (!pos) return false;
	const row = this.data[pos.y];
	return row && (pos.x in row);
};

DistanceMap.prototype.get = function(pos)
{
	const row = this.data[pos.y];
	return row ? row[pos.x] : undefined;
};

DistanceMap.prototype.remove = function(pos)
{
	const row = this.data[pos.y];
	if (row)
	{
		if (pos.x in row)
		{
			delete row[pos.x];
			if (_.isEmpty(row)) delete this.data[pos.y];
		}
	}
};

DistanceMap.prototype.copy = function()
{
	const result = new DistanceMap(this.roomName);
	for (const posVal of this) result.set(posVal.roomPos, posVal.value);
	return result;
};

DistanceMap.prototype.clear = function()
{
	this.data = {};
};

DistanceMap.prototype.positions = function*()
{
	for (const rowNr in this.data)
	{
		const row = this.data[rowNr];
		for (const colNr in row)
		{
			yield new RoomPosition(colNr, rowNr, this.roomName);
		}
	}
};

DistanceMap.prototype[Symbol.iterator] = function*()
{
	for (const roomPos of this.positions())
	{
		yield {roomPos: roomPos, value: this.get(roomPos)};
	}
};

DistanceMap.prototype.goAround = function*(start)
{
	for (const pos of util.goAround(start))
	{
		if (this.contains(pos)) yield pos;
	}
};

DistanceMap.prototype.reduce = function(callback)
{
	const iterator = this[Symbol.iterator]();
	let item = iterator.next();
	if (item.done) return {positions: []};
	const result = {value: item.value.value,  positions: [item.value.roomPos]};
	while (item = iterator.next(), !item.done)
	{
		const posVal = item.value;
		if (callback(result.value, posVal.value))
		{
			result.value = posVal.value;
			result.positions = [posVal.roomPos];
		}
		else if (result.value == posVal.value)
		{
			result.positions.push(posVal.roomPos);
		}
	}
	return result;
};

DistanceMap.prototype.filter = function(callback)
{
	const result = new DistanceMap(this.roomName);
	for (const rowNr in this.data)
	{
		const row = this.data[rowNr];
		for (const colNr in row)
		{
			if (callback(colNr, rowNr, row[colNr]))
			{
				result.set({ x: colNr, y: rowNr }, row[colNr]);
			}
		}
	}
	return result;
};

DistanceMap.prototype.clip = function(minDistance)
{
	return this.filter((x, y, value) => value >= minDistance);
};

DistanceMap.exitSeeds = function(roomName)
{
	const exitSeeds = new DistanceMap(roomName);
	for (let index = 0; index < util.roomSize.width; index++)
	{
		exitSeeds.set({ x: index, y: 0 }, true); // top row
		exitSeeds.set({ x: index, y: util.roomSize.height - 1 }, true); // bottom row
		exitSeeds.set({ x: 0, y: index }, true); // left column
		exitSeeds.set({ x: util.roomSize.width - 1, y: index }, true); // right column
	}
	return exitSeeds;
};

// Use surroundSeeds = true if the seeds themselves are not walkable
DistanceMap.floodfill = function(areaMap, roomName, seeds, surroundSeeds)
{
	let queue = [];
	for (const row in seeds)
	{
		for (const col in seeds[row])
		{
			if (surroundSeeds)
			{
				util.around(new RoomPosition(col, row, roomName)).forEach(function(pos)
				{
					queue.push({x: pos.x, y: pos.y, distance: 0});
				});
			}
			else queue.push({x: col, y: row, distance: 0});
		}
	}
	let distanceMap = new DistanceMap(roomName);
	while (queue.length > 0)
	{
		let cursor = queue.shift();
		const terrain = areaMap.get(cursor);
		if (terrain !== "#" &&
			(
				!distanceMap.contains(cursor) ||
				cursor.distance < distanceMap.get(cursor)
			))
		{
			distanceMap.set(cursor, cursor.distance);
			const nextDistance = cursor.distance + (terrain === " " ? 1 : CONSTRUCTION_COST_ROAD_SWAMP_RATIO);
			const around = util.around(new RoomPosition(cursor.x, cursor.y, roomName));
			for (let index = 0; index < around.length; index++)
			{
				const roomPos = around[index];
				queue.push({x: roomPos.x, y: roomPos.y, distance: nextDistance});
			}
		}
	}
	return distanceMap;
};

DistanceMap.prototype.average = function()
{
	let count = 0;
	let sumX = 0;
	let sumY = 0;
	for (const rowKey in this.data)
	{
		const row = this.data[rowKey];
		const rowNr = parseInt(rowKey);
		for (const colKey in row)
		{
			sumY += rowNr;
			sumX += parseInt(colKey);
			count++;
		}
	}
	return new RoomPosition(Math.round(sumX / count), Math.round(sumY / count), this.roomName);
};

DistanceMap.prototype.nearest = function(position)
{
	for (const pos of util.goAround(position))
	{
		if (this.contains(pos))
		{
			return pos;
		}
	}
};

DistanceMap.prototype.connectedComponents = function()
{
	let result = [];

	for (const rowNr in this.data)
	{
		const row = this.data[rowNr];
		for (const colNr in row)
		{
			// Find out if this entry is part of any of the known connected components
			let haveIt = false;
			for (let i = 0; i < result.length; i++)
			{
				if (result[i].contains({x:colNr, y:rowNr}))
				{
					haveIt = true;
					break;
				}
			}
			if (!haveIt)
			{
				// This entry is a new connected component. Collect the whole thing.
				let newComponent = new DistanceMap(this.roomName);
				let queue = [new RoomPosition(colNr, rowNr, this.roomName)];
				while (queue.length > 0)
				{
					const front = queue.shift();
					if (!newComponent.contains(front))
					{
						// We didn't see this entry yet. Copy it from the original into the connected component
						newComponent.set(front, this.get(front));
						// Also propagate the fringe
						const that = this;
						util.around(front).forEach(function(pos)
						{
							if (that.contains(pos) && !newComponent.contains(pos)) queue.push(pos);
						});
					}
				}
				result.push(newComponent);
			}
		}
	}
	return result;
};

DistanceMap.prototype.searchAway = function(startPos, targetDistance)
{
	let fringeDistance = this.get(startPos);
	const initialResult = {x: startPos.x, y: startPos.y, distance: fringeDistance};
	let queue = [initialResult];
	let results = [initialResult]; // Create this initial result just in case we can't do any better

	while (queue.length > 0)
	{
		const front = queue.shift();
		// Make sure we get all equally good results before we return the real results or update the temporary ones
		if (front.distance > fringeDistance)
		{
			if (fringeDistance >= targetDistance) return results;
			fringeDistance = front.distance;
			results = [front];
		}
		else results.push(front);

		// Enqueue only positions that are further away. Equal distance will already be on the queue.
		const that = this;
		util.around(new RoomPosition(front.x, front.y, this.roomName)).forEach(function(neighbour)
		{
			const dist = that.get(neighbour.x, neighbour.y);
			if (dist > fringeDistance) queue.push({x: neighbour.x, y: neighbour.y, distance: dist});
		});
	}
	// We kept intermediate results so even if we didn't reach the target distance then at least we can return the best ones so far.
	return results;
};

module.exports = DistanceMap;