const util = require("util");
const AreaMap = require("prototype.areamap");
const DistanceMap = require("prototype.distancemap");
// We want the maximum number of certain structures in our base, but only some or none of others
// 60 Extenstions
// 10 Labs
// 6 Towers
// 3 Spawns
// One of: link, storage, observer, powerspawn, terminal, nuker
// Total: 60 + 10 + 6 + 3 + 6
const FULL_BASE = 85;
// Build our base this far from all exits
const SAFE_DISTANCE = 20;

var RoomPlanner = function(where, showDebug)
{
	if (where instanceof RoomPosition)
	{
		this.spawnLocation = where;
		this.roomName = where.roomName;
	}
	else this.roomName = where;
	this.showDebug = showDebug;
	this.roomMem = util.valueIfExists("rooms", "seen", this.roomName);
	if (!this.roomMem) console.log("Trying to plan for unseen room");

	const exitSeeds = DistanceMap.exitSeeds(this.roomName);
	this.areaMap = new AreaMap(this.roomName, this.roomMem.map);
	this.exitMap = DistanceMap.floodfill(this.areaMap, this.roomName, exitSeeds.data);
};

RoomPlanner.prototype.plan = function(overrideSteps)
{
	if (this.showDebug) delete Memory.visuals;
	let bestArea;
	let anchor;
	if (this.spawnLocation)
	{
		bestArea = this.getAreaWith(this.spawnLocation);
		anchor = this.getAnchorNextTo(this.spawnLocation);
	}
	else
	{
		bestArea = this.getBestArea();
		anchor = this.getAnchor(bestArea);
	}
	const plan = this.getPlan(bestArea, anchor, overrideSteps);
	if (this.showDebug) this.show(plan);
	return plan;
};

RoomPlanner.prototype.getBestArea = function()
{
	const interestPoints = this.getInterestPoints();
	let best = undefined;
	for (const area of this.exitMap.clip(SAFE_DISTANCE).connectedComponents())
	{
		const evaluation = this.evaluateArea(area, interestPoints);
		if (!best || evaluation.score > best.score)
		{
			best = evaluation;
			best.area = area;
		}
	}
	if (!best)
	{
		// No areas were far away enough from the exit. If we have to build anywhere might as well
		// start building on the single spot that is furthest away.
		for (const posVal of this.exitMap)
		{
			if (!best || posVal.value > best.value) best = posVal;
		}
		const safestArea = new DistanceMap(this.roomName);
		safestArea.set(best.roomPos, true);
		best = this.evaluateArea(safestArea, interestPoints);
		best.area = safestArea;
	}
	return best;
};

RoomPlanner.prototype.getAreaWith = function(roomPos)
{
	const distance = Math.min(this.exitMap.get(roomPos), SAFE_DISTANCE);
	for (const area of this.exitMap.clip(distance).connectedComponents())
	{
		if (area.contains(roomPos)) return {area: area};
	}
};

RoomPlanner.prototype.getAnchorNextTo = function(roomPos)
{
	let best = {dist: 0};
	for (const pos of util.around(roomPos))
	{
		const dist = this.exitMap.get(pos);
		if (dist && dist > best.dist)
		{
			best = {dist: dist, pos: pos};
		}
	}
	return best.pos;
};

RoomPlanner.prototype.getInterestPoints = function()
{
	const interestPoints = [];
	for (const id in this.roomMem.sources)
	{
		const source = this.roomMem.sources[id];
		interestPoints.push(new RoomPosition(source.x, source.y, this.roomName));
	}
	for (const id in this.roomMem.minerals)
	{
		const mineral = this.roomMem.minerals[id];
		interestPoints.push(new RoomPosition(mineral.x, mineral.y, this.roomName));
	}
	const rc = this.roomMem.controller;
	if (rc) interestPoints.push(new RoomPosition(rc.x, rc.y, this.roomName));

	return interestPoints;
};

RoomPlanner.prototype.evaluateArea = function(area, interestPoints)
{
	const averagePosition = area.average();
	const avgInMap = area.nearest(averagePosition);
	const areaSize = area.size();
	let report = "sz:";
	if (this.showDebug) report += util.width(areaSize, 3) + " mid:" + avgInMap;
	const routes = [];
	const distances = [];
	for (const pos of interestPoints)
	{
		const route = PathFinder.search(avgInMap, { pos: pos, range: 1 }, { maxRooms: 1 });
		routes.push(route);
		distances.push(route.cost);
		if (this.showDebug) report += " " + util.width(route.cost, 3);
	}
	const score = areaSize / util.median(distances);
	if (this.showDebug)
	{
		report += " | median: " + util.median(distances) + " \t Score: " + score;
		console.log(report);
	}
	return {score: score, routes: routes};
};

RoomPlanner.prototype.getAnchor = function(evaluatedArea)
{
	const gates = [];
	for (const route of evaluatedArea.routes)
	{
		let gate = undefined;
		for (const point of route.path)
		{
			if (evaluatedArea.area.contains(point)) gate = point;
			else break;
		}
		if (gate) gates.push(gate);
		else gates.push(evaluatedArea.area.nearest(evaluatedArea.area.average()));
		if (this.showDebug)
		{
			const polys = util.ensureArray("visuals", this.roomName, "polys");
			const circles = util.ensureArray("visuals", this.roomName, "circles");
			if (gate) circles.push({ x: gate.x, y: gate.y, style: { radius: 0.15, fill: util.hue(0.2) } });
			polys.push({ points: route.path, style: { stroke: util.hue(0) } });
		}
	}
	return evaluatedArea.area.nearest(util.averagePosition(gates));
};

RoomPlanner.prototype.getPlan = function(bestArea, anchor, overrideSteps)
{
	const anchorSeed = new DistanceMap(this.roomName);
	anchorSeed.set(anchor, true);
	const anchorMap = DistanceMap.floodfill(this.areaMap, this.roomName, anchorSeed.data);
	const buildArea = bestArea.area;
	// These two distancemaps will hold assigned road and building locations
	const roads = new DistanceMap(this.roomName);
	const buildings = new DistanceMap(this.roomName);
	// These distancemaps are used in growing the roads and buildings maps
	const reach = new DistanceMap(this.roomName);
	const reachUpdates = new DistanceMap(this.roomName);
	const fringe = new DistanceMap(this.roomName);
	const reservedRoads = new DistanceMap(this.roomName);

	// Get initial values into the roads, reach and fringe maps
	{
		for (const point of this.getInterestPoints())
		{
			for (const neighbour of util.around(point))
			{
				if (!this.areaMap.isWall(neighbour)) reservedRoads.set(neighbour, true);
			}
		}
		const roundIt = util.goAround(anchor);
		roads.set(roundIt.next().value, true);
		for (let count = 0; count < 8; count++)
		{
			const position = roundIt.next().value;
			if (!this.areaMap.isWall(position)) reachUpdates.set(position, true);
		}
		for (let count = 0; count < 16; count++)
		{
			const position = roundIt.next().value;
			if (!this.areaMap.isWall(position)) fringe.set(position, true);
		}
		for (const pos of reachUpdates.positions()) reach.set(pos, this.getReachValue(pos, anchorMap, buildArea, fringe));
	}

	// Start growing the area by expanding the road and assigning building spots
	let step = 1;
	let nBuildings = 0;
	if (this.spawnLocation)
	{
		reach.remove(this.spawnLocation);
		buildings.set(this.spawnLocation, true);
		nBuildings++;
	}
	while (overrideSteps ? step < overrideSteps : nBuildings < FULL_BASE)
	{
		let best = undefined;
		for (const posVal of reach)
		{
			if (!best || posVal.value > best.value) best = posVal;
		}
		if (this.showDebug) console.log("best was " + JSON.stringify(best));
		const newRoads = [best.roomPos];
		for (const pos of util.around(best.roomPos))
		{
			// If the new road touches any of the reserved roads then include the whole reserved road
			if (reservedRoads.contains(pos))
			{
				// Find out which part of the reserved roads we need
				for (const subRoad of reservedRoads.connectedComponents())
				{
					if (subRoad.contains(pos))
					{
						// And include it
						for (const roadPos of subRoad.positions())
						{
							reservedRoads.remove(roadPos);
							newRoads.push(roadPos);
						}
					}
				}
			}
		}
		for (const newRoadPos of newRoads)
		{
			roads.set(newRoadPos, true);
			reach.remove(newRoadPos);
			// If a stretch of road is added then it may go through the fringe
			fringe.remove(newRoadPos);
		}
		// All positions bordering the new road are now in reach. Mark them to have their values updated
		reachUpdates.clear();
		for (const newRoadPos of newRoads)
		{
			for (const pos of util.around(newRoadPos))
			{
				// Most of the surrounding positions were part of the reach already, in which case we can directly include them to be updated.
				// If it's not in reach already we must check that the position is available.
				if (reach.contains(pos) || !roads.contains(pos) && !buildings.contains(pos) && !this.areaMap.isWall(pos)) reachUpdates.set(pos, true);
				// Either way, if the position was part of the fringe then it should check for reach positions that may no longer border the new fringe
				if (fringe.contains(pos))
				{
					fringe.remove(pos);
					for (const neighbour of util.around(pos))
					{
						if (reach.contains(neighbour)) reachUpdates.set(neighbour, true);
					}
				}
			}
		}
		// Update the fringe
		const fringeChecks = new DistanceMap(this.roomName);
		for (const pos of reachUpdates.positions())
		{
			for (const neighbour of util.around(pos))
			{
				fringeChecks.set(neighbour, true);
			}
		}
		for (const check of fringeChecks.positions())
		{
			if (!this.areaMap.isWall(check) &&
				!roads.contains(check) &&
				!buildings.contains(check) &&
				!reach.contains(check) &&
				!reachUpdates.contains(check))
			{
				fringe.set(check, true);
			}
		}
		// Now that the new fringe is complete we can compute the new reach values.
		for (const pos of reachUpdates.positions())
		{
			const newValue = this.getReachValue(pos, anchorMap, buildArea, fringe);
			if (newValue === undefined)
			{
				reach.remove(pos);
				buildings.set(pos, true);
				nBuildings++;
			}
			else reach.set(pos, newValue);
		}
		if (this.showDebug)
		{
			console.log(reach.size() + " tiles in reach, " + fringe.size() + " in fringe, " + buildings.size() + " building spots (" + nBuildings + ")");
			console.log((reach.size() + buildings.size()) + " total spots");
		}
		step++;
	}
	return {anchor: anchor, anchorMap: anchorMap, exitMap: this.exitMap, roads: roads, buildings: buildings, reach: reach, fringe: fringe};
};

RoomPlanner.prototype.getReachValue = function(point, anchorMap, buildArea, fringe)
{
	let value = 0.5 * this.exitMap.get(point);
	value -= anchorMap.get(point);
	if (!buildArea.contains(point)) value -= 100;
	let nFringeNeighbours = 0;
	for (const neighbour of util.around(point))
	{
		if (fringe.contains(neighbour))
		{
			nFringeNeighbours++;
		}
	}
	if (nFringeNeighbours == 0) return undefined;
	value += 2 * nFringeNeighbours;
	return value;
};

RoomPlanner.prototype.show = function(plan)
{
	let circles = util.ensureArray("visuals", this.roomName, "circles");
	
	for (const point of plan.roads.positions())
	{
		circles.push({ x: point.x, y: point.y, style: { radius: 0.2, fill: "#FFF", opacity: 0.75 } });
	}
	for (const point of plan.buildings.positions())
	{
		circles.push({ x: point.x, y: point.y, style: { radius: 0.52, fill: "#AA0", opacity: 0.1 } });
	}
	for (const point of plan.fringe.positions())
	{
		circles.push({ x: point.x, y: point.y, style: { radius: 0.15, fill: util.hue(0.8) } });
	}
	let minVal = 0;
	let maxVal = -Infinity;
	for (const posVal of plan.reach)
	{
		minVal = Math.min(minVal, posVal.value);
		maxVal = Math.max(maxVal, posVal.value);
	}
	if (maxVal > 0) minVal = 0;
	for (const posVal of plan.reach)
	{
		const value = (posVal.value - minVal) / (maxVal - minVal);
		circles.push({ x: posVal.roomPos.x, y: posVal.roomPos.y, style: { radius: 0.2 + Math.max(0, value * 0.3), fill: util.hue(value * 0.8) } });
	}
};

module.exports = RoomPlanner;