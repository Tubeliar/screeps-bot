const util = require("util");
const roads = require("manager.roads");

module.exports =
{
	spawn: function()
	{
		if (Memory.config.enableExpansion && Memory.expandTo && Memory.specialistCount.claimer < 1)
		{
			const roomName = Memory.expandTo.roomName;
			const roomInfo = Memory.rooms.seen[roomName];
			if (!roomInfo) return;
			const name = util.spawn([CLAIM, MOVE], undefined, {specialism: "claimer"});
			if (name)
			{
				console.log("Spawned " + name + " to claim " + Memory.expandTo);
			}
		}
	},
	
	run: function(creep)
	{
		if (Memory.expandTo)
		{
			if (creep.room.name == Memory.expandTo.roomName)
			{
				const controller = creep.room.controller;
				// TODO: Use boolean Memory.expandTo.reserve
				if (controller.my)
				{
					delete Memory.expandTo;
					// creep.suicide();
				}
				else
				{
					creep.moveTo(controller, {reusePath: 10});
					const result = creep.claimController(controller);
				}
			}
			else
			{
				const controllerInfo = util.valueIfExists("rooms", "seen", Memory.expandTo.roomName, "controller");
				if (!controllerInfo) delete Memory.expandTo;
				const pos = new RoomPosition(controllerInfo.x, controllerInfo.y, Memory.expandTo.roomName);
				creep.moveTo(pos, {costCallback: roads.moveCautious});
			}
		}
	}
};