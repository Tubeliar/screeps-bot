const util = require("util");
const stories = require("stories");

module.exports =
{
	spawn: function()
	{
		if (Memory.config.enableScout && Memory.specialistCount.scout < 1)
		{
			util.spawn([MOVE], undefined, {specialism: "scout"});
		}
	},
	
	run: function(creep)
	{
		if (!stories.tell(creep) && Math.random() < 0.1) stories.talkAbout(creep, "travel");
		if (!creep.memory.scoutRoom)
		{
			const neighbours = {};
			for (const roomName in Memory.rooms.owned)    this.getRoomOptions(roomName, neighbours);
			for (const roomName in Memory.rooms.reserved) this.getRoomOptions(roomName, neighbours);
			creep.memory.scoutRoom = _.sample(Object.keys(neighbours));
		}
		creep.moveTo(new RoomPosition(25, 25, creep.memory.scoutRoom), {reusePath: 10});
		if (creep.pos.roomName == creep.memory.scoutRoom || util.sameRoomPos(creep.memory.lastPos, creep.pos)) delete creep.memory.scoutRoom;
	},
	
	getRoomOptions: function(roomName, neighbours)
	{
		const coords = util.roomCoords(roomName);
		if (neighbours == undefined) neighbours = {};
		// Vary the radius so that nearby rooms are visited more often
		const radius = Math.min(Memory.config.scoutRadius, Math.ceil(1 / Math.random() - 1));
		for (let dy = -radius; dy <= radius; dy++)
		{
			for (let dx = -radius; dx <= radius; dx++)
			{
				if (dx == 0 && dy == 0) continue;
				const roomName = util.roomName(coords.x + dx, coords.y + dy);
				if (Game.map.getRoomStatus(roomName).status == "normal" &&
					!Game.rooms[roomName] &&
					!Memory.rooms.owned[roomName] &&
					!Memory.rooms.reserved[roomName])
				{
					neighbours[roomName] = true;
				}
			}
		}
		return neighbours;
	}
};