

module.exports =
{
	tell: function(creep)
	{
		const story = creep.memory.story;
		if (!story) return false;
		const storyIndex = creep.memory.storyIndex;
		const line = story[storyIndex];
		if (line.length > 0) creep.say(line, true);
		if (storyIndex < story.length - 1)
		{
			creep.memory.storyIndex = storyIndex + 1;
		}
		else
		{
			creep.memory.story = undefined;
			creep.memory.storyIndex = undefined;
		}
		return true;
	},
	
	talkAbout: function(creep, topic, silentTicks = 3)
	{
		const story = _.sample(this[topic]);
		if (story.length > creep.ticksToLive - 10) return;
		creep.memory.story = story.concat(Array(silentTicks).fill(""));
		creep.memory.storyIndex = 0;
	},
	
	travel:
	[
		["Adventure" , "is"        , "worthwile."],
		["Not"       , "all those" , "who wander", "are lost."],
		["Take only" , "memories," , "leave only", "footprints"],
		["Travel is" , "glamorous" , "only in"   , "retrospect"],
		["Traveling" , "tends to"  , "magnify"   , "all human" , "emotions."],
		["Life is"   , "either"    , "a daring"  , "adventure" , "or nothing"],
		["Fear makes", "strangers" , "of people" , "who should", "be friends"],
		["Happiness" , "is a way"  , "of travel" , "– not a"   , "dstination"],
		["Wherever"  , "you go,"   , "go with"   , "all"       , "your heart"],
		["I have"    , "not been"  , "everywhere", "but it’s"  , "on my list"],
		["Life"      , "begins at" , "the end"   , "of your"   , "comfort"   , "zone."],
		["A journey" , "is best"   , "measured"  , "in friends", "instead of", "miles."],
		["No place"  , "is ever"   , "as bad as" , "they say"  , "it’s going", "to be."],
		["Once each" , "year, go"  , "someplace" , "you have"  , "never been", "before."],
		["Travel"    , "and change", "of place"  , "impart new", "vigor to"  , "the mind."],
		["It is not" , "down in"   , "any map;"  , "true"      , "places"    , "never are."],
		["A journey" , "of"        , "a thousand", "miles must", "begin with", "a single"  , "step."],
		["He who"    , "would"     , "travel"    , "happily"   , "must"      , "travel"    , "light."],
		["Travel"    , "is the"    , "only thing", "you buy"   , "that"      , "makes you" , "richer."],
		["The world" , "is a book,", "and those" , "who do not", "travel"    , "read only" , "one page."],
		["There are" , "no foreign", "lands."    , "It is the" , "traveler"  , "only who"  , "is foreign"],
		["We travel,", "some of us", "forever,"  , "to seek"   , "new places", "new lives,", "new souls."],
		["We travel" , "not to"    , "escape"    , "life, but" , "for life"  , "not to"    , "escape us."],
		["One’s"     , "dstination", "is never"  , "a place,"  , "but a new" , "way of"    , "seeing"    , "things."],
		["A good"    , "traveler"  , "has no"    , "fixed"     , "plans, and", "is not"    , "intent on" , "arriving."],
		["Man cannot", "discover"  , "new oceans", "unless he" , "has the"   , "courage to", "lose sight", "of shore."],
		["Tourists"  , "don’t know", "where they", "have been,", "travelers" , "don’t know", "where they", "are going."],
		["Two roads" , "diverged"  , "in a wood" , "and I –"   , "I took"    , "the one"   , "less"      , "traveled"  , "by."],
		["All"       , "journeys"  , "have"      , "secret"    , "desti-"    , "nations of", "which the" , "traveler"  , "is unaware"],
		["I have"    , "seen more" , "than I"    , "remember," , "and"       , "remember"  , "more"      , "than I"    , "have seen."],
		["To travel" , "is to"     , "discover"  , "that"      , "everyone"  , "is wrong"  , "about"     , "other"     , "countries."],
		["The"       , "traveler"  , "sees what" , "he sees,"  , "the"       , "tourist"   , "sees what" , "he has"    , "come"      , "to see"],
		["A man"     , "travels"   , "the world" , "over"      , "in search" , "of what he", "needs and" , "returns"   , "home to"   , "find it."],
		["When"      , "abroad"    , "you learn" , "more about", "your own"  , "country,"  , "than"      , "you do"    , "the place" , "you are"   , "visiting."],
		["Do not"    , "follow"    , "where"     , "the path"  , "may lead." , "Go instead", "where"     , "there is"  , "no path"   , "and leave" , "a trail."],
		["I see"     , "my path"   , "but i do"  , "not know"  , "where"     , "it leads." , "This"      , "is what"   , "inspires"  , "me to"     , "travel it."],
		["If you"    , "reject"    , "the food," , "ignore the", "customs,"  , "fear the"  , "religion"  , "and avoid" , "the people", "you might" , "better", "stay at", "home."],
		["Our"       , "happiest"  , "moments as", "tourists"  , "always"    , "seem to"   , "come when" , "we stumble", "upon one"  , "thing"     , "while in", "pursuit of", "something", "else."]
	]
};