
module.exports =
{
	TaskPickup: require("task.pickup"),
	TaskHarvest: require("task.harvest"),
	TaskDeliver: require("task.deliver"),
	TaskBuild: require("task.build"),
	TaskUpgrade: require("task.upgrade"),
	TaskStore: require("task.store")
};