const util = require("util");
const Task = require("task");

let TaskBuild = function(id, pos, amount, done, priority)
{
	Task.call(this, "Build", id, pos, done);
	this.amount = amount;
	this.priority = priority;
};

TaskBuild.prototype = Object.create(Task.prototype);
TaskBuild.prototype.constructor = TaskBuild;

TaskBuild.prototype.getFitnessEx = function(creep, target, repeat, inProgress)
{
	const energyToGive = creep.willCarry.energy || 0;
	if (energyToGive <= 0) return 0;
	
	const fillRatio = energyToGive / Math.min(creep.carryCapacity, this.amount);
	if (inProgress) return 1 + fillRatio;
	
	const workPower = 1 - 1 / (creep.memory.body[WORK] + 1);
	
	const almostFinishedBoost = 0.1 / this.amount;
	return util.rangeFactor(creep, this.pos) * workPower * fillRatio + almostFinishedBoost + (repeat ? this.repeatBonus : 0) + (this.priority ? 0.2 : 0);
};

TaskBuild.prototype.adjust = function(creep)
{
	const energyToGive = creep.willCarry.energy || 0;
	const newAmount = this.amount - energyToGive;
	if (newAmount > 0) return new TaskBuild(this.targetId, this.pos, newAmount, true, this.priority);
	else return null;
};

TaskBuild.prototype.performAction = function(creep, target)
{
	const result = creep.build(target);
	if (result == OK)
	{
		const amount = creep.memory.body[WORK] * BUILD_POWER;
		creep.willCarry.energy -= amount;
		// TODO: target.futureEnergy += amount;
	}
	return result;
};


module.exports =
{
	getTasks: function(roomName)
	{
		const tasks = [];
		const allSites = util.valueIfExists("constructionSites", roomName) || [];
		for (const site of allSites)
		{
			if (!site.id) continue;
			const pos = new RoomPosition(site.x, site.y, roomName);
			tasks.push(new TaskBuild(site.id, pos, site.progressTotal - site.progress, false, site.type == STRUCTURE_SPAWN));
		}
		return tasks;
	},
	
	revive: function(memory)
	{
		return new TaskBuild(memory.targetId, new RoomPosition(memory.pos.x, memory.pos.y, memory.pos.roomName), memory.amount, memory.done, memory.priority);
	}
};