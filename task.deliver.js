const util = require("util");
const Task = require("task");

let TaskDeliver = function(id, pos, amount)
{
	Task.call(this, "Bring", id, pos);
	this.amount = amount;
};

TaskDeliver.prototype = Object.create(Task.prototype);
TaskDeliver.prototype.constructor = TaskDeliver;

TaskDeliver.prototype.getFitnessEx = function(creep, target, repeat, inProgress)
{
	const energyToGive = creep.willCarry.energy || 0;
	if (!energyToGive) return 0;
	const fillRatio = Math.min(1, energyToGive / Math.min(creep.carryCapacity, this.amount));
	return util.rangeFactor(creep, target.pos) * fillRatio * 1.2 + (repeat ? this.repeatBonus : 0);
};

TaskDeliver.prototype.adjust = function(creep)
{
	const energyToGive = creep.willCarry.energy || 0;
	const newAmount = this.amount - energyToGive;
	if (newAmount > 0) return new TaskDeliver(this.targetId, this.pos, newAmount);
	else return null;
};

TaskDeliver.prototype.performAction = function(creep, target)
{
	const amount = Math.min(Math.max(0, target.energyCapacity - target.futureEnergy), creep.willCarry.energy || 0);
	const result = creep.transfer(target, RESOURCE_ENERGY, amount);
	if (result == OK)
	{
		creep.willCarry.energy -= amount;
		target.futureEnergy += amount;
	}
	return result;
};

module.exports =
{
	getTasks: function(roomName)
	{
		const tasks = [];
		const targets = Game.rooms[roomName].find(FIND_STRUCTURES, {filter: (structure) =>
		{
			if (structure.futureEnergy >= structure.energyCapacity) return false;
			return (structure.structureType == STRUCTURE_EXTENSION ||
					structure.structureType == STRUCTURE_SPAWN ||
					structure.structureType == STRUCTURE_POWER_SPAWN ||
					structure.structureType == STRUCTURE_TOWER);
		}});
		for (let index = 0; index < targets.length; index++)
		{
			const target = targets[index];
			const amount = target.energyCapacity - target.futureEnergy;
			tasks.push(new TaskDeliver(target.id, target.pos, amount));
		}
		return tasks;
	},
	
	revive: function(memory)
	{
		return new TaskDeliver(memory.targetId, new RoomPosition(memory.pos.x, memory.pos.y, memory.pos.roomName), memory.amount);
	}
};