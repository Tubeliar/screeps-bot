const util = require("util");
const Task = require("task");
const sourceMan = require("manager.sources");

let TaskHarvest = function(id, pos, slots, goalRate)
{
	Task.call(this, "Reap ", id, pos, goalRate <= 10);
	this.slots = slots;
	this.goalRate = goalRate;
};

TaskHarvest.prototype = Object.create(Task.prototype);
TaskHarvest.prototype.constructor = TaskHarvest;

TaskHarvest.prototype.getFitnessEx = function(creep, target, repeat, inProgress)
{
	const emptiness = 1 - _.sum(creep.willCarry) / creep.carryCapacity;
	if (emptiness <= 0) return 0;
	if (inProgress && target.energy > 0) return 1 + emptiness;
	
	// 1 if the creep's harvest power exactly matches the goal rate. Approaches 0 the more goalRate is lower than the harvesting power
	let rateMatch = 1;
	const harvestPower = creep.memory.body[WORK] * HARVEST_POWER;
	if (this.goalRate < harvestPower)
	{
		const rateDiff = Math.abs(this.goalRate - harvestPower);
		rateMatch = 5 / (rateDiff + 5);
	}
	return util.rangeFactor(creep, target.pos) * emptiness * rateMatch + (repeat ? this.repeatBonus : 0);
};

TaskHarvest.prototype.adjust = function(creep)
{
	if (this.slots < 2) return null;
	const newGoalRate = this.goalRate - creep.memory.body[WORK] * HARVEST_POWER;
	return new TaskHarvest(this.targetId, this.pos, this.slots - 1, newGoalRate);
};

TaskHarvest.prototype.performAction = function(creep, target)
{
	const result = creep.harvest(target);
	if (result == OK)
	{
		const amount = creep.memory.body[WORK] * HARVEST_POWER;
		const remainingCapacity = _.sum(creep.willCarry);
		creep.willCarry.energy += Math.min(remainingCapacity, amount);
		// TODO: source -= amount
	}
	return result;
};

module.exports =
{
	getTasks: function(roomName)
	{
		const tasks = [];
		const roomSources = sourceMan.getSources(roomName);
		for (const id in roomSources)
		{
			// TODO: Generate tasks for rooms in which we have no vision
			const source = Game.getObjectById(id);
			if (source && source.energy > 0)
			{
				const ticksToRegen = source.ticksToRegeneration || ENERGY_REGEN_TIME;
				const goalRate = source.energy / (Math.max(1, ticksToRegen - 25));
				tasks.push(new TaskHarvest(id, source.pos, roomSources[id].harvestPositions.length, goalRate));
			}
		}
		return tasks;
	},
	
	revive: function(memory)
	{
		return new TaskHarvest(memory.targetId, new RoomPosition(memory.pos.x, memory.pos.y, memory.pos.roomName), memory.slots, memory.goalRate);
	}
};