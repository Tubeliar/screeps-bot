const util = require("util");
const roadManager = require("manager.roads");

const Task = function(sayVerb, targetId, roomPos, done = false)
{
	this.targetId = targetId;
	this.sayVerb = sayVerb;
	this.pos = roomPos;
	this.done = done;
	this.class = this.__proto__.constructor.name;
};

Task.revive = function(memory)
{
	return new Task(memory.sayVerb, memory.targetId, new RoomPosition(memory.pos.x, memory.pos.y, memory.pos.roomName), memory.done);
};

Task.prototype.repeatBonus = 0.1;

Task.prototype.getFitness = function(creep)
{
	const cTask = creep.memory.currentTask;
	const target = Game.getObjectById(this.targetId);
	if (cTask)
	{
		if (cTask.class == this.class && cTask.targetId == this.targetId)
		{
			return this.getFitnessEx(creep, target, true, cTask.inProgress);
		}
	}
	return this.getFitnessEx(creep, target, false, false);
};

Task.prototype.getFitnessEx = function(creep, target, repeat, inProgress)
{
	if (inProgress) return 1;
	return util.rangeFactor(creep, this.pos) + (repeat ? this.repeatBonus : 0);
};

Task.prototype.adjust = function(creep)
{
	return this;
};

Task.prototype.say = function(creep)
{
	const posName = (this.pos.roomName == creep.room.name) ? this.pos.x + "," + this.pos.y : this.pos.roomName;
	creep.say(this.sayVerb + posName);
};

Task.prototype.perform = function(creep)
{
	const target = Game.getObjectById(this.targetId);
	let result = ERR_NOT_IN_RANGE;
	if (target)
	{
		result = this.performAction(creep, target);
		creep.memory.currentTask.inProgress = result == OK;
		if (result == ERR_NOT_IN_RANGE)
		{
			this.repairAround(creep);
		}
		else if (result != OK)
		{
			//console.log(creep.name + "[" + creep.carry.energy + "] (" + creep.pos.x + "," + creep.pos.y + ") could not " + this.sayVerb + ": " + util.errorString(result));
		}
	}
	if (creep.name == Memory.config.monitor) console.log(Game.time + " " + creep.name + "[" + creep.carry.energy + "]" + creep.pos + " does " + this.sayVerb + " " + this.pos +": " + util.errorString(result));
	return result;
};

Task.prototype.performAction = function(creep, target)
{
	return ERR_INVALID_TARGET;
};

Task.prototype.prepare = function(creep)
{
	if (creep.name == Memory.config.monitor) console.log(Game.time + " " + creep.name + "[" + creep.carry.energy + "]" + creep.pos + " moves for " + this.sayVerb);
	creep.moveTo(this.pos, { costCallback: roadManager.moveCautious, reusePath: 6, ignoreCreeps: /*creep.pos.getRangeTo(this.pos.x, this.pos.y) > 2*/ false});
};

Task.prototype.repairAround = function(creep)
{
	if (creep.carry.energy <= 0) return;
	let mostNeedStruct = null;
	let mostNeed = -1;
	const structures = creep.pos.findInRange(FIND_STRUCTURES, 3);
	structures.forEach(function(structure)
	{
		let need = 1 - structure.hits / structure.hitsMax;
		if (structure instanceof StructureRoad) need *= roadManager.importance(structure.pos);
		else if (structure instanceof StructureWall) need = 0;
		if (need > 0 && need > mostNeed)
		{
			mostNeed = need;
			mostNeedStruct = structure;
		}
	});
	
	if (mostNeedStruct && mostNeed > 0)
	{
		const result = creep.repair(mostNeedStruct);
		if (result != OK) console.log(creep.name + "(" + creep.pos.x + "," + creep.pos.y + ") could not fix " + JSON.stringify(mostNeedStruct) + ": " + util.errorString(result));
		//creep.say("Fix " + mostNeedStruct.pos.x + "," + mostNeedStruct.pos.y);
	}
};

module.exports = Task;