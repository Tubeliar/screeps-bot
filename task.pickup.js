const Task = require("task");

let TaskPickup = function(id, pos, amount)
{
	Task.call(this, "Grab ", id, pos);
	this.amount = amount;
};

TaskPickup.prototype = Object.create(Task.prototype);
TaskPickup.prototype.constructor = TaskPickup;

TaskPickup.prototype.getFitness = function(creep)
{
	const capacity = creep.carryCapacity - _.sum(creep.willCarry);
	if (capacity <= 0) return 0;
	
	// Pickup this resource if it gets us more energy than we could harvest
	// in the time it takes us to go and get it
	const target = Game.getObjectById(this.targetId);
	const range = creep.pos.getRangeTo(target.pos);
	const energyGained = Math.min(capacity, this.amount - range + 1);
	const harvestPower = creep.memory.body[WORK] * HARVEST_POWER;
	return energyGained / Math.max(1, (range - 1) * harvestPower);
};
	
TaskPickup.prototype.adjust = function(creep)
{
	const capacity = creep.carryCapacity - _.sum(creep.willCarry);
	const newAmount = this.amount - capacity;
	if (newAmount > 0) return new TaskPickup(this.targetId, newAmount);
	else return null;
};

TaskPickup.prototype.performAction = function(creep, target)
{
    let result = OK;
    if (target instanceof Tombstone)
    {
        const resource_type = _.max(Object.keys(target.store), key => target.store[key]);
        result = creep.withdraw(target, resource_type);
    }
    else result = creep.pickup(target);
	if (result == OK)
	{
		const remainingCapacity = _.sum(creep.willCarry);
		const amount = Math.min(remainingCapacity, target.amount);
		creep.willCarry[target.resourceType] = (creep.willCarry[target.resourceType] || 0) + amount;
		// TODO: target.... -= amount;
	}
	return result;
};

module.exports =
{
	getTasks: function(roomName)
	{
		const tasks = [];
		Game.rooms[roomName].find(FIND_DROPPED_RESOURCES).forEach(function(target)
		{
			if (target.resourceType == RESOURCE_ENERGY || target.room.find(FIND_MY_STRUCTURES, { filter: s => s.structureType == STRUCTURE_STORAGE }).length > 0)
			{
				tasks.push(new TaskPickup(target.id, target.pos, target.amount));
			}
		});
		Game.rooms[roomName].find(FIND_TOMBSTONES).forEach(function(tomb)
		{
		    const amount = tomb.store.getUsedCapacity();
			if (amount > 0) tasks.push(new TaskPickup(tomb.id, tomb.pos, amount));
		});
		return tasks;
	},
	
	revive: function(memory)
	{
		return new TaskPickup(memory.targetId, new RoomPosition(memory.pos.x, memory.pos.y, memory.pos.roomName), memory.amount);
	}
};