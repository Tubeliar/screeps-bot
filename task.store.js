const util = require("util");
const Task = require("task");

let TaskStore = function(id, pos)
{
	Task.call(this, "Store", id, pos, true);
};

TaskStore.prototype = Object.create(Task.prototype);
TaskStore.prototype.constructor = TaskStore;

TaskStore.prototype.getFitness = function(creep)
{
	const amount = _.sum(creep.willCarry) - (creep.willCarry.energy || 0);
	if (amount <= 0) return 0;
	return util.rangeFactor(creep, Game.getObjectById(this.targetId).pos);
};

TaskStore.prototype.performAction = function(creep, target)
{
	let result = undefined;
	let type = undefined;
	for (const resource in creep.willCarry)
	{
		if (resource != RESOURCE_ENERGY)
		{
			result = creep.transfer(target, resource, creep.willCarry[resource]);
			type = resource;
			break;
		}
	}
	if (result == OK)
	{
		delete creep.willCarry[type];
	}
	return result;
};

module.exports =
{
	getTasks: function(roomName)
	{
		const tasks = [];
		for (const target of Game.rooms[roomName].find(FIND_MY_STRUCTURES, {filter: {structureType:"storage"}}))
		{
			tasks.push(new TaskStore(target.id, target.pos));
		}
		return tasks;
	},
	
	revive: function(memory)
	{
		return new TaskStore(memory.targetId, new RoomPosition(memory.pos.x, memory.pos.y, memory.pos.roomName));
	}
};