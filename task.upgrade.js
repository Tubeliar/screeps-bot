const util = require("util");
const Task = require("task");

let TaskUpgrade = function(id, pos, urgence)
{
	Task.call(this, "Raise", id, pos, urgence < 0.5);
	this.urgence = urgence;
};

TaskUpgrade.prototype = Object.create(Task.prototype);
TaskUpgrade.prototype.constructor = TaskUpgrade;

TaskUpgrade.prototype.getFitnessEx = function(creep, target, repeat, inProgress)
{
	const fillRatio = (creep.carry.energy || 0) / creep.carryCapacity;
	if (fillRatio <= 0) return 0;
	if (inProgress) return 1 + fillRatio;
	
	return Math.max(0.01, util.rangeFactor(creep, target.pos) * fillRatio * 0.2) + 1.8 * this.urgence + (repeat ? this.repeatBonus : 0);
};
   
TaskUpgrade.prototype.adjust = function(creep)
{
	return new TaskUpgrade(this.targetId, this.pos, 0);
};

TaskUpgrade.prototype.performAction = function(creep, target)
{
	const result = creep.upgradeController(target);
	if (result == OK)
	{
		creep.signController(creep.room.controller, "Hi!");
		const amount = creep.memory.body[WORK] * UPGRADE_CONTROLLER_POWER;
		creep.willCarry.energy -= amount;
	}
	return result;
};


module.exports =
{
	getTasks: function(roomName)
	{
		const tasks = [];
		const controller = Game.rooms[roomName].controller;
		if (controller)
		{
			const downgradeDuration = CONTROLLER_DOWNGRADE[controller.level];
			// The urgence may become very small but it must never be 0
			const urgence = 1 - (controller.ticksToDowngrade / (downgradeDuration + 1));
			tasks.push(new TaskUpgrade(controller.id, controller.pos, urgence));
		}
		return tasks;
	},
	
	revive: function(memory)
	{
		return new TaskUpgrade(memory.targetId, new RoomPosition(memory.pos.x, memory.pos.y, memory.pos.roomName), memory.urgence);
	}
};