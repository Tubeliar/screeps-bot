module.exports =
{
	same: function(desired, actual)
	{
		if (!(typeof desired === "string" || desired instanceof String))
		{
			desired = JSON.stringify(desired);
		}
		if (!(typeof actual === "string" || actual instanceof String))
		{
			actual = JSON.stringify(actual);
		}
		let failed = (desired === actual) ? 0 : 1;
		if (failed)
		{
			console.log("FAIL: " + desired + " != " + actual);
		}
		return failed;
	},

	report: function(name, failed)
	{
		if (failed == 0)
		{
			console.log(name + " OK");
		}
		else
		{
			console.log(name + " failed " + failed + " test" + (failed > 1 ? "s" : ""));
		}
		return failed;
	}
};