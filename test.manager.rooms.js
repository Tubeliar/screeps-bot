const roomMan = require("manager.rooms");
const test = require("test");

module.exports =
{
	run: function()
	{
		this.readAreaMap();
	},

	readAreaMap: function()
	{
		let fail = 0;
		// W3N5
		const map = "####                                         #####\n###.                                            ##\n##..          ##                    # .          #\n##            ##     .              #...         #\n##                                    ....        \n#                                    ......       \n#                                    .  .......   \n#                        ..               . ..    \n##                       ..                       \n###                       .              #        \n####            .         ..                  #.  \n#####           .          .                      \n######   .      ####                          .   \n#     #  ..    #####   ..                         \n#      #       ###                                \n       ##      ##                                 \n     ..##     ###        ##                       \n      .###    ####      ####              ..      \n      ####    ######    ###               ...     \n      ####     #####    ###               ...     \n      ####     #####                              \n      ####      ###                     #         \n#      ####      #                      ##        \n#       ###                             .         \n#      ####                                . ##   \n# . #######                                 ####  \n# .########                             .    ##   \n#   #  ####                 #          ##     ..  \n#                          ###         ##     ..  \n#                           ##                ..  \n   ..        ###                              .   \n   ####     ####         ...                  .   \n   #####   ####### ...                           #\n   #################..                    ##     #\n       #####  #######                     ###    #\n  ..     #     ##                         ###    #\n              #  ..                        #    ##\n#            ###         #                     ###\n#           ####        ###                    ###\n      ..   .####...     ##                     .##\n          ..####...                       #    . #\n            ####..                  ##   ###     #\n            ####                    ##  ####     #\n            ####  ##               ###  ###      #\n#    ##     #########              ##   ###      #\n#    ###   ##########       .          ####      #\n#     ##############                   ####      #\n##.    #############            ##     ####      #\n##### ##############        #######   ######    ##\n##################################################";
		fail += test.same("#", roomMan.readAreaMap(map, 0, 0));
		fail += test.same(".", roomMan.readAreaMap(map, 3, 1));
		fail += test.same(" ", roomMan.readAreaMap(map, 13, 2));
		fail += test.same(" ", roomMan.readAreaMap(map, 15, 11));
		fail += test.same(".", roomMan.readAreaMap(map, 16, 11));
		fail += test.same(" ", roomMan.readAreaMap(map, 15, 12));
		fail += test.same("#", roomMan.readAreaMap(map, 16, 12));
		fail += test.same(".", roomMan.readAreaMap(map, 47, 39));
		fail += test.same("#", roomMan.readAreaMap(map, 48, 39));
		fail += test.same(".", roomMan.readAreaMap(map, 47, 40));
		fail += test.same(" ", roomMan.readAreaMap(map, 48, 40));
		fail += test.same(" ", roomMan.readAreaMap(map, "48", "40"));
		return test.report("readAreaMap", fail);
	}
};