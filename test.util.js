const util = require("util");
const test = require("test");

module.exports =
{
	run: function()
	{
		this.mrr();
		this.roomRange();
		this.roomCoords();
		this.roomName();
		this.hue();
		this.makeHex();
		this.goAround();
	},

	mrr: function()
	{
		let fail = 0;
		fail += test.same( "22", util.mrr(new RoomPosition(25, 25, "W1N1"), new RoomPosition( 3,  3, "W1N1")));
		fail += test.same( "22", util.mrr(new RoomPosition( 3,  3, "W1N1"), new RoomPosition(25, 25, "W1N1")));
		fail += test.same( "38", util.mrr(new RoomPosition( 5, 23, "W0N1"), new RoomPosition(17, 23, "W1N1")));
		fail += test.same( "10", util.mrr(new RoomPosition(45, 25, "W1N1"), new RoomPosition( 5, 25, "W0N1")));
		fail += test.same( "72", util.mrr(new RoomPosition(25, 29, "W1N0"), new RoomPosition(25,  7, "W1N1")));
		fail += test.same( "65", util.mrr(new RoomPosition(11, 30, "W0N0"), new RoomPosition(45, 15, "W1N1")));
		fail += test.same("126", util.mrr(new RoomPosition(13, 37, "E1N1"), new RoomPosition(37, 28, "W1N1")));
		fail += test.same("147", util.mrr(new RoomPosition(17, 41, "W1S1"), new RoomPosition(23, 44, "W1N1")));
		fail += test.same("156", util.mrr(new RoomPosition(19, 43, "E1S1"), new RoomPosition(14, 37, "W1N1")));
		fail += test.same("134", util.mrr(new RoomPosition(23, 47, "W1N1"), new RoomPosition( 7,  6, "E1S1")));
		return test.report("mrr", fail);
	},

	roomRange: function()
	{
		let fail = 0;
		fail += test.same("0", util.roomRange(new RoomPosition(25, 25, "W1N1"), new RoomPosition( 3,  3, "W1N1")));
		fail += test.same("0", util.roomRange(new RoomPosition( 3,  3, "W1N1"), new RoomPosition(25, 25, "W1N1")));
		fail += test.same("1", util.roomRange(new RoomPosition( 5, 23, "W0N1"), new RoomPosition(17, 23, "W1N1")));
		fail += test.same("1", util.roomRange(new RoomPosition(45, 25, "W1N1"), new RoomPosition( 5, 25, "W0N1")));
		fail += test.same("1", util.roomRange(new RoomPosition(25, 29, "W1N0"), new RoomPosition(25,  7, "W1N1")));
		fail += test.same("2", util.roomRange(new RoomPosition(11, 30, "W0N0"), new RoomPosition(45, 15, "W1N1")));
		fail += test.same("3", util.roomRange(new RoomPosition(13, 37, "E1N1"), new RoomPosition(37, 28, "W1N1")));
		fail += test.same("3", util.roomRange(new RoomPosition(17, 41, "W1S1"), new RoomPosition(23, 44, "W1N1")));
		fail += test.same("6", util.roomRange(new RoomPosition(19, 43, "E1S1"), new RoomPosition(14, 37, "W1N1")));
		fail += test.same("6", util.roomRange(new RoomPosition(23, 47, "W1N1"), new RoomPosition( 7,  6, "E1S1")));
		return test.report("roomRange", fail);
	},

	roomCoords: function()
	{
		let fail = 0;
		fail += test.same("{\"x\":0,\"y\":0}", util.roomCoords("E0S0"));
		fail += test.same("{\"x\":1,\"y\":0}", util.roomCoords("E1S0"));
		fail += test.same("{\"x\":0,\"y\":1}", util.roomCoords("E0S1"));
		fail += test.same("{\"x\":1,\"y\":1}", util.roomCoords("E1S1"));
		fail += test.same("{\"x\":-1,\"y\":0}", util.roomCoords("W0S0"));
		fail += test.same("{\"x\":0,\"y\":-1}", util.roomCoords("E0N0"));
		fail += test.same("{\"x\":-1,\"y\":-1}", util.roomCoords("W0N0"));
		fail += test.same("{\"x\":-2,\"y\":0}", util.roomCoords("W1S0"));
		fail += test.same("{\"x\":0,\"y\":-2}", util.roomCoords("E0N1"));
		return test.report("roomCoords", fail);
	},

	roomName: function()
	{
		let fail = 0;
		fail += test.same("E0S0", util.roomName( 0,  0));
		fail += test.same("E1S0", util.roomName( 1,  0));
		fail += test.same("E0S1", util.roomName( 0,  1));
		fail += test.same("E1S1", util.roomName( 1,  1));
		fail += test.same("W0S0", util.roomName(-1,  0));
		fail += test.same("E0N0", util.roomName( 0, -1));
		fail += test.same("W0N0", util.roomName(-1, -1));
		fail += test.same("W1S0", util.roomName(-2,  0));
		fail += test.same("E0N1", util.roomName( 0, -2));
		return test.report("roomName", fail);
	},

	hue: function()
	{
		let fail = 0;
		fail += test.same("#666666", util.hue(-10));
		fail += test.same("#ff0000", util.hue(0.0));
		fail += test.same("#ff9900", util.hue(0.1));
		fail += test.same("#ccff00", util.hue(0.2));
		fail += test.same("#33ff00", util.hue(0.3));
		fail += test.same("#00ff66", util.hue(0.4));
		fail += test.same("#00ffff", util.hue(0.5));
		fail += test.same("#0066ff", util.hue(0.6));
		fail += test.same("#3300ff", util.hue(0.7));
		fail += test.same("#cc00ff", util.hue(0.8));
		fail += test.same("#ff0099", util.hue(0.9));
		fail += test.same("#ff0000", util.hue(1.0));
		fail += test.same("#aaaaaa", util.hue(1.1));
		fail += test.same("#aaaaaa", util.hue(10));
		return test.report("hue", fail);
	},

	makeHex: function()
	{
		let fail = 0;
		fail += test.same("00", util.makeHex(0.0));
		fail += test.same("1a", util.makeHex(0.1));
		fail += test.same("33", util.makeHex(0.2));
		fail += test.same("4d", util.makeHex(0.3));
		fail += test.same("66", util.makeHex(0.4));
		fail += test.same("99", util.makeHex(0.6));
		fail += test.same("b3", util.makeHex(0.7));
		fail += test.same("cc", util.makeHex(0.8));
		fail += test.same("e6", util.makeHex(0.9));
		fail += test.same("ff", util.makeHex(1.0));
		return test.report("makeHex", fail);
	},

	goAround: function()
	{
		let fail = 0;
		let generator = util.goAround(new RoomPosition(10, 10, "test"));
		fail += test.same(new RoomPosition(10, 10, "test"), generator.next().value);
		
		fail += test.same(new RoomPosition(10,  9, "test"), generator.next().value);
		fail += test.same(new RoomPosition( 9, 10, "test"), generator.next().value);
		fail += test.same(new RoomPosition(11, 10, "test"), generator.next().value);
		fail += test.same(new RoomPosition(10, 11, "test"), generator.next().value);
		fail += test.same(new RoomPosition( 9,  9, "test"), generator.next().value);
		fail += test.same(new RoomPosition( 9, 11, "test"), generator.next().value);
		fail += test.same(new RoomPosition(11,  9, "test"), generator.next().value);
		fail += test.same(new RoomPosition(11, 11, "test"), generator.next().value);
		
		fail += test.same(new RoomPosition(10,  8, "test"), generator.next().value);
		fail += test.same(new RoomPosition( 8, 10, "test"), generator.next().value);
		fail += test.same(new RoomPosition(12, 10, "test"), generator.next().value);
		fail += test.same(new RoomPosition(10, 12, "test"), generator.next().value);
		fail += test.same(new RoomPosition(11,  8, "test"), generator.next().value);
		fail += test.same(new RoomPosition( 8,  9, "test"), generator.next().value);
		fail += test.same(new RoomPosition(12, 11, "test"), generator.next().value);
		fail += test.same(new RoomPosition( 9, 12, "test"), generator.next().value);
		return test.report("goAround", fail);
	}
};