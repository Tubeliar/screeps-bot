const roomSize = { width: 50, height: 50 };
// We always want as many of these as we can get
const BUILD_ALL =
	[
		STRUCTURE_SPAWN, STRUCTURE_EXTENSION,
		STRUCTURE_TOWER, STRUCTURE_LAB
	];
// We only ever want one of these. We're only allowed one mostly, but still...
const BUILD_ONE =
	[
		STRUCTURE_STORAGE, STRUCTURE_POWER_SPAWN,
		STRUCTURE_TERMINAL, STRUCTURE_LINK,
		STRUCTURE_OBSERVER, STRUCTURE_NUKER
	];
const BUILD_ORDER = BUILD_ALL.concat(BUILD_ONE);
const ROAD_BUILD_THRESHOLD = 25;

const names1 = ["Jackson", "Aiden", "Liam", "Lucas", "Noah", "Mason", "Jayden", "Ethan", "Jacob", "Jack", "Caden", "Logan", "Benjamin", "Michael", "Caleb", "Ryan", "Alexander", "Elijah", "James", "William", "Oliver", "Connor", "Matthew", "Daniel", "Luke", "Brayden", "Jayce", "Henry", "Carter", "Dylan", "Gabriel", "Joshua", "Nicholas", "Isaac", "Owen", "Nathan", "Grayson", "Eli", "Landon", "Andrew", "Max", "Samuel", "Gavin", "Wyatt", "Christian", "Hunter", "Cameron", "Evan", "Charlie", "David", "Sebastian", "Joseph", "Dominic", "Anthony", "Colton", "John", "Tyler", "Zachary", "Thomas", "Julian", "Levi", "Adam", "Isaiah", "Alex", "Aaron", "Parker", "Cooper", "Miles", "Chase", "Muhammad", "Christopher", "Blake", "Austin", "Jordan", "Leo", "Jonathan", "Adrian", "Colin", "Hudson", "Ian", "Xavier", "Camden", "Tristan", "Carson", "Jason", "Nolan", "Riley", "Lincoln", "Brody", "Bentley", "Nathaniel", "Josiah", "Declan", "Jake", "Asher", "Jeremiah", "Cole", "Mateo", "Micah", "Elliot"];
const names2 = ["Sophia", "Emma", "Olivia", "Isabella", "Mia", "Ava", "Lily", "Zoe", "Emily", "Chloe", "Layla", "Madison", "Madelyn", "Abigail", "Aubrey", "Charlotte", "Amelia", "Ella", "Kaylee", "Avery", "Aaliyah", "Hailey", "Hannah", "Addison", "Riley", "Harper", "Aria", "Arianna", "Mackenzie", "Lila", "Evelyn", "Adalyn", "Grace", "Brooklyn", "Ellie", "Anna", "Kaitlyn", "Isabelle", "Sophie", "Scarlett", "Natalie", "Leah", "Sarah", "Nora", "Mila", "Elizabeth", "Lillian", "Kylie", "Audrey", "Lucy", "Maya", "Annabelle", "Makayla", "Gabriella", "Elena", "Victoria", "Claire", "Savannah", "Peyton", "Maria", "Alaina", "Kennedy", "Stella", "Liliana", "Allison", "Samantha", "Keira", "Alyssa", "Reagan", "Molly", "Alexandra", "Violet", "Charlie", "Julia", "Sadie", "Ruby", "Eva", "Alice", "Eliana", "Taylor", "Callie", "Penelope", "Camilla", "Bailey", "Kaelyn", "Alexis", "Kayla", "Katherine", "Sydney", "Lauren", "Jasmine", "London", "Bella", "Adeline", "Caroline", "Vivian", "Juliana", "Gianna", "Skyler", "Jordyn"];

module.exports =
{
	roomSize,
	BUILD_ALL,
	BUILD_ONE,
	BUILD_ORDER,
	ROAD_BUILD_THRESHOLD,

	ensure: function(str)
	{
		const arr = arguments.length == 1 ? str.split(".") : arguments;
		let obj = Memory;
		for (let index = 0; index < arr.length; index++)
		{
			const nextLevel = obj[arr[index]];
			if (nextLevel) obj = nextLevel;
			else obj = obj[arr[index]] = {};
		}
		return obj;
	},

	ensureArray: function()
	{
		let obj = Memory;
		for (let index = 0; index < arguments.length - 1; index++)
		{
			const nextLevel = obj[arguments[index]];
			if (nextLevel) obj = nextLevel;
			else obj = obj[arguments[index]] = {};
		}
		const last = arguments[arguments.length -1];
		if (!Array.isArray(obj[last])) obj[last] = [];
		return obj[last];
	},
	
	sameRoomPos: function(roomPos, memoryPos)
	{
		if (!roomPos || !memoryPos) return false;
		return memoryPos.x == roomPos.x &&
		       memoryPos.y == roomPos.y &&
		       memoryPos.roomName == roomPos.roomName;
	},
	
	valueIfExists: function()
	{
		let obj = Memory;
		for (let index = 0; index < arguments.length; index++)
		{
			const nextKey = arguments[index];
			if (nextKey in obj) obj = obj[nextKey];
			else return undefined;
		}
		return obj;
	},
	
	width: function(input, size)
	{
		const str = input.toString();
		if (str.length == size) return str;
		
		const isNum = typeof input == "number";
		const isDecimal = isNum && str.indexOf(".") > -1;
		if (str.length > size)
		{
			if (isNum && !isDecimal) return str.slice(-size);
			else return str.slice(0, size);
		}
		const pad = " ".repeat(size - str.length);
		if (isNum) return pad + str;
		else return str + pad;
	},
	
	around: function(roomPos)
	{
		const positions = [];
		const yRange = {min: Math.max(0, roomPos.y - 1), max: Math.min(roomSize.height - 1, roomPos.y + 1)};
		const xRange = {min: Math.max(0, roomPos.x - 1), max: Math.min(roomSize.width - 1, roomPos.x + 1)};
		for (let y = yRange.min; y <= yRange.max; y++)
		{
			for (let x = xRange.min; x <= xRange.max; x++)
			{
				if (x == roomPos.x && y == roomPos.y) continue;
				positions.push(new RoomPosition(x, y, roomPos.roomName));
			}
		}
		return positions;
	},

	growBlock: function*(roomPos, width, height)
	{
		if (!height) height = width;
		const xs = width  < 0 ? -1 : 1;
		const ys = height < 0 ? -1 : 1;
		const absWidth = xs * width;
		const absHeight = ys * height;
		for (let edge = 0; edge < Math.max(absWidth, absHeight); edge++)
		{
			if (edge < absWidth)
			{
				for (let dy = 0; dy < Math.min(absHeight, edge); dy++)
				{
					yield this.relativePosition(roomPos, xs * edge, ys * dy);
				}
			}
			if (edge < absHeight)
			{
				for (let dx = 0; dx < Math.min(absWidth, edge + 1); dx++)
				{
					yield this.relativePosition(roomPos, xs * dx, ys * edge);
				}
			}
		}
	},
	
	relativePosition: function(roomPos, dx, dy, clamp = false)
	{
		let x = roomPos.x + dx;
		if (clamp)
		{
			if (x < 0) x = 0;
			else if (x >= roomSize.width) x = roomSize.width - 1;
		}
		else if (x < 0 || x >= roomSize.width) return null;
		
		let y = roomPos.y + dy;
		if (clamp)
		{
			if (y < 0) y = 0;
			if (y >= roomSize.height) y = roomSize.height - 1;
		}
		else if (y < 0 || y >= roomSize.height) return null;
		return new RoomPosition(x, y, roomPos.roomName);
	},

	getRandomName: function (prefix)
	{
		let name;
		const nameArray = Math.random() > .5 ? names1 : names2;
		for (let tries = 0; tries < 3; tries++)
		{
			name = nameArray[Math.floor(Math.random() * nameArray.length)];
			if (prefix) name = prefix + name;
			if (Game.creeps[name] === undefined) break;
		}
		if (Game.creeps[name] !== undefined) name += nameArray[Math.floor(Math.random() * nameArray.length)];
		return name;
	},

	spawn: function (parts, roomName, memory = {})
	{
		let spawns;
		const room = roomName ? Game.rooms[roomName] : null;
		if (room) spawns = room.find(FIND_MY_STRUCTURES, { filter: { structureType: STRUCTURE_SPAWN } });
		if (!spawns || spawns.length == 0) spawns = Game.spawns;
		spawns = _.shuffle(spawns);

		if (roomName) memory.home = roomName;
		for (const spawnIndex in spawns)
		{
			const spawn = spawns[spawnIndex];
			const name = this.getRandomName();
			const result = spawn.spawnCreep(parts, name, {memory: memory});
			const report = result != spawn.memory.lastResult;
			spawn.memory.lastResult = result;
			if (result == ERR_BUSY) continue;
			if (result == OK) return name;
			if (report)
			{
				console.log(spawn.pos + " " + spawn.name + " can't spawn: " + this.errorString(result));
				if (result == ERR_INVALID_TARGET) console.log("for " + JSON.stringify(parts));
			}
		}
		return null;
	},

	makeFlagRoute: function(fromRoomPos, toRoomPos)
	{
		const found = PathFinder.search(fromRoomPos, {"pos": toRoomPos, "range": 1});
		let counter = 0;
		for (const key in found.path)
		{
			this.makeFlag(found.path[key], counter++ / (found.path.length - 1));
		}
	},
	
	makeFlags: function(room, data, highestValue)
	{
		if (highestValue < 1) highestValue = 1;
		for (const row in data)
		{
			for (const column in data[row])
			{
				this.makeFlag(room.getPositionAt(column, row), data[row][column] / highestValue);
			}
		}
	},
	
	makeFlag: function(roomPos, value)
	{
		const room = Game.rooms[roomPos.roomName];
		if (!room) return;
		
		const colors = value.primary ? value : this.ratioToFlagColors(value);
		const flag = Game.flags[roomPos.toString()];
		if (flag)
		{
			flag.setColor(colors.primary, colors.secondary);
		}
		else room.createFlag(roomPos, roomPos.toString(), colors.primary, colors.secondary);
	},
	
	averagePosition: function(positions)
	{
		let x = 0;
		let y = 0;
		let roomName = false;
		for (let index = 0; index < positions.length; index++)
		{
			let pos = positions[index];
			if (pos.pos) pos = pos.pos;
			if (!roomName && pos.roomName) roomName = pos.roomName;
			
			x += pos.x;
			y += pos.y;
		}
		if (!roomName) roomName = "unknown";
		return new RoomPosition(Math.round(x / positions.length), Math.round(y / positions.length), roomName);
	},
	
	medianPosition: function(positions)
	{
		const xValues = [];
		const yValues = [];
		let roomName = false;
		for (let index = 0; index < positions.length; index++)
		{
			let pos = positions[index];
			if (pos.pos) pos = pos.pos;
			if (!roomName && pos.roomName) roomName = pos.roomName;
			
			xValues.push(pos.x);
			yValues.push(pos.y);
		}
		return new RoomPosition(Math.round(this.median(xValues)), Math.round(this.median(yValues)), roomName);
	},

	average: function(values)
	{
		if (!values || !values.length) return 0;

		let sum = 0;
		for (const value of values) sum += value;
		return sum / values.length;
	},
	
	median: function(values)
	{
		if (!values || !values.length) return 0;
		
		const sorted = values.sort();
		const middle = values.length / 2;
		if (values.length % 2 == 0) return (sorted[middle - 1] + sorted[middle]) / 2;
		else return sorted[Math.floor(middle - 1)];
	},
	
	ratioToFlagColors: function(ratio)
	{
		let sec = 1;
		if (ratio < 1)
		{
			sec = ratio * 10;
			sec -= Math.floor(sec);
		}
		return {primary: this.ratioToFlagColor(ratio), secondary: this.ratioToFlagColor(sec)};
	},
	
	ratioToFlagColor: function(ratio)
	{
		if (ratio < 0.2) return COLOR_BROWN;
		if (ratio < 0.4) return COLOR_RED;
		if (ratio < 0.6) return COLOR_ORANGE;
		if (ratio < 0.8) return COLOR_YELLOW;
		return COLOR_WHITE;
	},
	
	clearFlags: function()
	{
		for (const name in Game.flags)
		{
			Game.flags[name].remove();
		}
	},

	eucDistanceSqr: function(roomPos1, roomPos2)
	{
		const rc1 = this.roomCoords(roomPos1.roomName);
		const rc2 = this.roomCoords(roomPos2.roomName);
		const dx = roomPos1.x - roomPos2.x + (rc1.x - rc2.x) * this.roomSize.width;
		const dy = roomPos1.y - roomPos2.y + (rc1.y - rc2.y) * this.roomSize.height;
		return dx * dx + dy * dy;
	},

	euclideanDistance: function (roomPos1, roomPos2)
	{
		return Math.sqrt(this.eucDistanceSqr(roomPos1, roomPos2));
	},
	
	mrr: function(roomPosFrom, roomPosTo)
	{
		// Multi Room Range. GetRangeTo returns 0 when source and target arent in the same room.
		if (roomPosFrom.roomName == roomPosTo.roomName) return roomPosFrom.getRangeTo(roomPosTo);
		const rcFrom = this.roomCoords(roomPosFrom);
		const rcTo = this.roomCoords(roomPosTo);
		return roomPosFrom.getRangeTo(roomPosTo.x + (rcTo.x - rcFrom.x) * roomSize.width, roomPosTo.y + (rcTo.y - rcFrom.y) * roomSize.height);
	},
	
	rangeFactor: function(creep, roomPos)
	{
		const range = this.mrr(creep.pos, roomPos);
		if (range >= creep.ticksToLive) return 0;
		return 11 / (range + 10);
	},
	
	roomRange: function(roomPos1, roomPos2)
	{
		if (roomPos1.roomName == roomPos2.roomName) return 0;
		const coord1 = this.roomCoords(roomPos1);
		const coord2 = this.roomCoords(roomPos2);
		return Math.abs(coord1.x - coord2.x) + Math.abs(coord1.y - coord2.y);
	},
	
	roomCoords: function(roomPosOrName)
	{
		if (roomPosOrName.pos) roomPosOrName = roomPosOrName.pos;
		const roomName = roomPosOrName.roomName || roomPosOrName.name || roomPosOrName;
		const parts = roomName.split(/[EWNS]/);
		let x = Number(parts[1]);
		let y = Number(parts[2]);
		if (roomName.includes("W")) x = -1 - x;
		if (roomName.includes("N")) y = -1 - y;
		return {x: x, y: y};
	},
	
	roomName: function(x, y)
	{
		let name = x < 0 ? "W" + (-1 - x) : "E" + x;
		name    += y < 0 ? "N" + (-1 - y) : "S" + y;
		return name;
	},
	
	closestSpawn: function(roomPosition)
	{
		let best = {spawn:null, range: Infinity};
		for (const spawnName in Game.spawns)
		{
			const range = this.mrr(Game.spawns[spawnName].pos, roomPosition);
			if (range < best.range)
			{
				best = {spawn: Game.spawns[spawnName], range: range};
			}
		}
		return best.spawn;
	},

	hue: function(h)
	{
		h *= 6;
		if (h < 0) return "#666666";
		if (h < 1) return "#ff" + this.makeHex(h) + "00";
		if (h < 2) return "#" + this.makeHex(2 - h) + "ff00";
		if (h < 3) return "#00ff" + this.makeHex(h - 2);
		if (h < 4) return "#00" + this.makeHex(4 - h) + "ff";
		if (h < 5) return "#" + this.makeHex(h - 4) + "00ff";
		if (h <= 6) return "#ff00" + this.makeHex(6 - h);
		return "#aaaaaa";
	},

	goAround: function*(pos, maxRadius = 25)
	{
		if (!("x" in pos && "y" in pos && "roomName" in pos))
		{
			throw new Error("goAround: Not a RoomPosition " + JSON.stringify(pos));
		}
		yield pos;
		for (let radius = 1; radius < maxRadius; radius++)
		{
			for (const p of this.goAcross(pos, radius, 0)) yield p;
			for (let offset = 1; offset < radius; offset++)
			{
				for (const p of this.goAcross(pos, radius, -offset)) yield p;
				for (const p of this.goAcross(pos, radius,  offset)) yield p;
			}
			for (const p of this.goAcross(pos, radius, radius)) yield p;
		}
	},

	goAcross: function*(pos, radius, offset)
	{
		yield new RoomPosition(pos.x - offset, pos.y - radius, pos.roomName);
		yield new RoomPosition(pos.x - radius, pos.y + offset, pos.roomName);
		yield new RoomPosition(pos.x + radius, pos.y - offset, pos.roomName);
		yield new RoomPosition(pos.x + offset, pos.y + radius, pos.roomName);
	},

	makeHex: function(ratio)
	{
		let hex = Math.round(ratio * 255).toString(16);
		if (hex.length < 2) hex = "0" + hex;
		return hex;
	},
	
	errorString: function(code)
	{
		switch(code)
		{
			case OK: return "OK";
			case ERR_NOT_OWNER: return "Not owner";
			case ERR_NO_PATH: return "no path";
			case ERR_NAME_EXISTS: return "name exists";
			case ERR_BUSY: return "busy";
			case ERR_NOT_FOUND: return "not found";
			case ERR_NOT_ENOUGH_ENERGY: return "not enough energy";
			case ERR_NOT_ENOUGH_RESOURCES: return "not enough resources";
			case ERR_INVALID_TARGET: return "invalid target";
			case ERR_FULL: return "full";
			case ERR_NOT_IN_RANGE: return "not in range";
			case ERR_INVALID_ARGS: return "invalid target";
			case ERR_TIRED: return "tired";
			case ERR_NO_BODYPART: return "no bodypart";
			case ERR_NOT_ENOUGH_EXTENSIONS: return "not enough extensions";
			case ERR_RCL_NOT_ENOUGH: return "RCL not enough";
			case ERR_GCL_NOT_ENOUGH: return "GCL not enough";
		}
		return "unknown code: " + code;
	},
	
	logGraph: function(data)
	{
		const maxValue = Math.max(1, _.max(data));
		const minValue = Math.min(0, _.min(data));
		const lines = 15;
		const samples = Math.min(data.length, 250);
		const yStep = (maxValue - minValue) / lines;
		const xStep = data.length / samples;
		let lastY = maxValue;
		console.log("Current value: " + data[data.length - 1]);
		for (let y = maxValue - yStep; y > minValue - yStep; y -= yStep)
		{
			const isAxis =  y <= 0 && 0 < lastY;
			let line = isAxis ? "+" : "|";
			for (let x = 0; x < data.length; x += xStep)
			{
				const value = data[Math.floor(x)];
				if (y <= value && value < lastY)
				{
					const characters = ["_", ",", "-", "`"];
					// There are 8 blocks if you count the empty block, but we want only visible signs
					line += characters[Math.floor(characters.length * (value - y) / yStep)];
				}
				else line += isAxis ? "." : " ";
			}
			console.log(line);
			lastY = y;
		}
	}
};